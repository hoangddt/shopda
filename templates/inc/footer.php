
        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-7">
                            <div class="block-subscribe">
                                <div class="newsletter">
                                    <form accept-charset='UTF-8' action='/contact' id='contact' method='post'>
                                        <input name='FormType' type='hidden' value='contact' />
                                        <input name='utf8' type='hidden' value='true' />
                                        <h4>Đăng kí nhận tin</h4>
                                        <input type="text" name="contact[email]" placeholder="Nhập email" class="input-text required-entry validate-email" title="Nhập email" id="newsletter1" value="">
                                        <button class="subscribe" title="Gửi đi" type="submit"><span>Gửi đi</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5">
                            <div class="social">
                                <ul>
                                    <li class="fb">
                                        <a href="1"></a>
                                    </li>
                                    <li class="tw">
                                        <a href="2"></a>
                                    </li>
                                    <li class="googleplus">
                                        <a href="Về chúng tôi"></a>
                                    </li>
                                    <li class="rss">
                                        <a href="3"></a>
                                    </li>
                                    <li class="pintrest">
                                        <a href="4"></a>
                                    </li>
                                    <li class="linkedin">
                                        <a href="6"></a>
                                    </li>
                                    <li class="youtube">
                                        <a href="5"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-middle container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="footer-logo">
                            <a class="logo" href="//polo-theme.bizwebvietnam.net">

                                <img alt="polo-theme" src="/shopda/images/logo.png" />

                            </a>
                        </div>
                        <p>Polo shop được thành lập từ 8/2010 được sự tin tưởng của khách hàng trong suốt thời gian hoạt động đến nay cửa hàng ngày một phát triển và xây dựng được thương hiệu trong lòng quý khách .</p>
                        <div class="payment-accept">
                            <div><img src="/shopda/images/payment-1.png" alt="payment"> <img src="/shopda/images/payment-2.png" alt="payment"> <img src="/shopda/images/payment-3.png" alt="payment"> <img src="/shopda/images/payment-4.png" alt="payment"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <h4>Về chúng tôi</h4>
                        <ul class="links">

                            <li><a href="/">Trang chủ</a></li>

                            <li><a href="/gioi-thieu">Giới thiệu</a></li>

                            <li><a href="/collections/all">Sản phẩm</a></li>

                            <li><a href="/tin-tuc">Tin tức</a></li>

                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <h4>Hỗ trợ</h4>
                        <ul class="links">

                            <li><a href="/search">Tìm kiếm</a></li>

                            <li><a href="/account/login">Đăng nhập</a></li>

                            <li><a href="/account/register">Đăng ký</a></li>

                            <li><a href="/cart">Giỏ hàng</a></li>

                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <h4>Hướng dẫn</h4>
                        <ul class="links">

                            <li><a href="/dieu-khoan">Điều khoản</a></li>

                            <li><a href="/huong-dan">Hướng dẫn</a></li>

                            <li><a href="/chinh-sach">Chính sách</a></li>

                            <li><a href="/lien-he">Liên hệ</a></li>

                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h4>Liên hệ</h4>
                        <div class="contacts-info">
                            <address>
						<i class="add-icon">&nbsp;</i>442 Đội Cấn - Ba Đình - Hà Nội
					</address>
                            <div class="phone-footer"><i class="phone-icon">&nbsp;</i> (04) 6674 2332</div>
                            <div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="mailto:support@bizweb.vn">support@bizweb.vn</a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 coppyright">&copy; Cung cấp bởi <a href="https://bizweb.vn" target="blank">Bizweb</a></div>
                        <div class="col-sm-7 col-xs-12 company-links">
                            <ul class="links">

                                <li><a href="/">Trang chủ</a></li>

                                <li><a href="/gioi-thieu">Giới thiệu</a></li>

                                <li><a href="/collections/all">Sản phẩm</a></li>

                                <li><a href="/tin-tuc">Tin tức</a></li>

                                <li><a href="/lien-he">Liên hệ</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    <script type="text/javascript">
        Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {

            if ((typeof cart_summary_id) === 'string') {
                var cart_summary = jQuery(cart_summary_id);
                if (cart_summary.length) {
                    // Start from scratch.
                    cart_summary.empty();
                    // Pull it all out.        
                    jQuery.each(cart, function(key, value) {
                        if (key === 'items') {

                            var table = jQuery(cart_summary_id);
                            if (value.length) {

                                jQuery('<div class="block-subtitle">Sản phẩm đã cho vào giỏ hàng</div>').appendTo(table)
                                jQuery.each(value, function(i, item) {
                                    jQuery('<li class="item even"> <a class="product-image" href="' + item.url + '"><img src="' + Bizweb.resizeImage(item.image, 'small') + '" width="80"></a><div class="detail-item"><div class="product-details"> <a href="javascript:void(0);" onclick="Bizweb.removeItem(' + item.variant_id + ')" title="xóa sản phẩm" onClick="" class="glyphicon glyphicon-remove">&nbsp;</a><p class="product-name"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a> </p></div><div class="product-details-bottom"> <span class="price">' + Bizweb.formatMoney(item.price, "{{amount_no_decimals_with_comma_separator}}&#8363;") + '</span> <span class="title-desc">Số lượng:</span> <strong>' + item.quantity + '</strong> </div></div></li>').appendTo(table);
                                });
                                jQuery('<div class="top-subtotal">Tổng tiền: <span class="price total_price">5.230.000&#8363;</span></div><div class="actions"><button class="btn-checkout" type="button"><a href="/checkout\"><span>Thanh toán</span></a></button><button class="view-cart" type="button"><a href="/cart\"><span>giỏ hàng</span></a></button></div>').appendTo(table);
                            } else {
                                jQuery('<div class="block-subtitle">Không có sản phẩm nào trong giỏ hàng.</div>').appendTo(table);
                            }
                        }
                    });
                }
            }
            updateCartDesc(cart);
        }


        function updateCartDesc(data) {
            var $cartLinkText = $('#open_shopping_cart'),
                $cartCount = $('#cart-total'),
                $cartPrice = Bizweb.formatMoney(data.total_price, "{{amount_no_decimals_with_comma_separator}}&#8363;");
            switch (data.item_count) {
                case 0:
                    $cartLinkText.attr('data-amount', '0');
                    $cartCount.text('0');
                    break;
                case 1:
                    $cartLinkText.attr('data-amount', '1');
                    $cartCount.text('1');
                    break;
                default:
                    $cartLinkText.attr('data-amount', data.item_count);
                    $cartCount.text(data.item_count);
                    break;
            }
            $('.total_price').html($cartPrice);
        }
        Bizweb.onCartUpdate = function(cart) {
            Bizweb.updateCartFromForm(cart, '.shopping_cart', 'shopping-cart');
        };
        $(window).load(function() {
            // Let's get the cart and show what's in it in the cart box.  
            Bizweb.getCart(function(cart) {
                Bizweb.updateCartFromForm(cart, '.shopping_cart');
            });
        });
    </script>
</body>
<?php
    ob_end_flush();
?>
</html>