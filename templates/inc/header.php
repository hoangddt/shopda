<!doctype html>
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js">
<!--<![endif]-->
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php
    
    ob_start( );
    include $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/define.php';
?>
<?php
    session_start();
?>
<head>
    <meta charset="utf-8" />
    <title>
       Shop hoa
    </title>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="polo-theme">
    <meta property="og:url" content="http: /">
    <meta property="og:site_name" content="polo-theme">
    <link rel="canonical" href="http: /">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <!-- fonts -->
    <!-- JavaScript -->
    <script src='/js/jquery-2.1.4.min.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/bootstrap.min.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/parallax.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/common.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/revslider.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/owl.carousel.min.js?1455850384069' type='text/javascript'></script>
	
    <script src='/shopda/js/jgrowl.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/api.jquery.js?137261' type='text/javascript'></script>
    <script src='/shopda/js/cs.script.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/main.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/libraries/ckeditor/ckeditor.js' type='text/javascript'></script>
    
    <!-- Styles -->
    <!-- CSS Style -->
    <link href='/shopda/css/bootstrap.min.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/revslider.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/owl.carousel.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/owl.theme.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/font-awesome.css?1455850384069' rel='stylesheet' type='text/css' />


	<link href='/shopda/templates/css/style.css' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/jgrowl.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/styles.css?1455850384069' rel='stylesheet' type='text/css' />
	<link href="/shopda//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Google Fonts -->
   
    <!-- Header hook for plugins ================================================== -->
    <script>
        var Bizweb = Bizweb || {};
        Bizweb.store = 'polo-theme.bizwebvietnam.net';
        Bizweb.theme = {
            "id": 48701,
            "name": "Polo",
            "role": "main",
            "previewable": true,
            "processing": false,
            "created_on": "2015-11-30T10:06:45Z",
            "modified_on": "2015-11-30T10:07:46Z"
        }
        Bizweb.template = 'index';
    </script>

    <script>
        //<![CDATA[
        (function() {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=7';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();

        //]]>
    </script>
    <noscript><iframe height='0' width='0' style='display:none;visibility:hidden' src=''></iframe></noscript>

    <script>
        (function() {
            function asyncLoad() {
                var urls = [];
                for (var i = 0; i < urls.length; i++) {
                    var s = document.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = urls[i];
                    s.src = urls[i];
                    var x = document.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }
            }
            window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
        })();
    </script>

    <script type='text/javascript'>
        (function() {
            var log = document.createElement('script');
            log.type = 'text/javascript';
            log.async = true;
            log.src = '//stats.bizweb.vn/delivery/37483.js?lang=vi';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(log, s);
        })();
    </script>
	
    <!-- Google Tag Manager -->
    <noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body class="">
    <div class="page">
        <header class="header-container">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <!-- Header Language -->
                        <div class="col-xs-6">
                            <div class="welcome-msg hidden-xs"> Chào mừng bạn tới Polo store </div>
                        </div>
                        <div class="col-xs-6">

                            <!-- Header Top Links -->
                            <div class="toplinks">
                                <div class="links">

                                    <div class="register"><a title="Register" href="login.php"><span  class="hidden-xs">Chào,
                                    <?php
                                        if(isset($_SESSION['dangnhap'])){
                                            echo $_SESSION['dangnhap']['tenkh'];
                                        }
                                        else
                                        {
                                            echo "Khách";
                                        }
                                    ?>
                                    !</span></a></div>
                                    <div class="login"><a title="Login" href="logout.php"><span  class="hidden-xs">Thoát</span></a></div>

                                </div>
                            </div>
                            <!-- End Header Top Links -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="header container">
                <div class="row">
                    <div class="col-lg-2 col-sm-3 col-md-2">
                        <!-- Header Logo -->
                        <a class="logo" href="">

                            <img alt="polo-theme" src="/shopda/images/logo.png" />

                        </a>
                        <!-- End Header Logo -->
                    </div>
                    <!-- Top Cart -->
                    
                    <!-- End Top Cart -->
                </div>
            </div>
        </header>
        <!-- end header -->
        <!-- Navbar -->
        <nav>
            <div class="container">
                <div class="nav-inner">
                    <div class="logo-small">
                        <a class="logo" href=" ">

                            <img alt="polo-theme" src="/images/logo.png" />

                        </a>
                    </div>
                    <!-- mobile-menu -->
                    <div class="hidden-desktop" id="mobile-menu">
                        <ul class="navmenu">
                            <li>
                                <div class="menutop">
                                    <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
                                    <h2>Danh mục</h2>
                                </div>
                                <ul class="submenu">
                                    <li>
                                        <ul class="topnav">



                                            <li class="level0 nav-8 level-top parent">
                                                <a class="level-top" href="index.php"> <span>Quản trị</span> </a>
                                                <ul class="level0">



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="danhmuc.php"> <span>Danh mục</span> </a>
                                                    </li>
                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="gioithieu.php"> <span>Giới thiệu</span> </a>
                                                    </li>



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="lienhe.php"> <span>Liên hệ</span> </a>
                                                    </li>



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="phantrang.php"> <span>Phân trang</span> </a>
                                                    </li>
													
													<li class="level1 nav-2-1 first parent">
                                                        <a href="quangcao.php"> <span>Quảng cáo</span> </a>
                                                    </li>
													
													<li class="level1 nav-2-1 first parent">
                                                        <a href="tintuc.php"> <span>Tin tức</span> </a>
                                                    </li>

                                                </ul>
                                            </li>
											
											<li class="level0 nav-8 level-top parent">
                                                <a class="level-top" href="donhang.php"> <span>Quản lý đơn hàng</span> </a>
                                                <ul class="level0">



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="danhmuc.php"> <span>Đơn hàng chưa duyệt</span> </a>
                                                    </li>
                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="gioithieu.php"> <span>Đơn hàng đã duyệt</span> </a>
                                                    </li>



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="lienhe.php"> <span>Đơn hàng chưa thanh toán</span> </a>
                                                    </li>



                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="phantrang.php"> <span>Đơn hàng đã thanh toán</span> </a>
                                                    </li>
													

                                                </ul>
                                            </li>
                                            <li class="level0 nacvv-8 level-top parent">
                                                <a class="level-top" href="gioithieu.php"> <span>Người dùng</span> </a>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--navmenu-->
                    </div>
                    <!--End mobile-menu -->
                    <ul id="nav" class="hidden-xs">
					
					
                        <li class="level0 parent drop-menu"><a href="index.php"><span>Quản trị</span></a>
                            <ul class="level1">



                                <li class="level1 first parent"><a href="danhmuc.php"><span>Danh mục</span></a></li>


                                <li class="level1 first parent"><a href="gioithieu.php"><span>Giới thiệu</span></a></li>



                                <li class="level1 first parent"><a href="lienhe.php"><span>Liên hệ</span></a></li>



                                <li class="level1 first parent"><a href="phantrang.php"><span>Phân trang</span></a></li>
								
								<li class="level1 nav-2-1 first parent">
									<a href="quangcao.php"> <span>Quảng cáo</span> </a>
								</li>
								
								<li class="level1 nav-2-1 first parent">
									<a href="tintuc.php"> <span>Tin tức</span> </a>
								</li>


                            </ul>
                        </li>
						
						<li class="level0 parent drop-menu"><a href="donhang.php"><span>Quản lý đơn hàng</span></a>
                            <ul class="level1">



                                <li class="level1 first parent"><a href="dhchuaduyet.php"><span>Đơn hàng chưa duyệt</span></a></li>


                                <li class="level1 first parent"><a href="dhdaduyet.php"><span>Đơn hàng đã duyệt</span></a></li>



                                <li class="level1 first parent"><a href="dhcthanhtoan.php"><span>Đơn hàng chưa thanh toán</span></a></li>



                                <li class="level1 first parent"><a href="dhdthanhtoan.php"><span>Đơn hàng đã thanh toán</span></a></li>
								
								


                            </ul>
                        </li>
						
						<li class="level0 parent drop-menu"><a href="gioithieu.php"><span>Người dùng</span></a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- end nav -->