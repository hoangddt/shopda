﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/checkuser.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/config.php';?>

<div id="subnav">
            <!-- End. .container_12 -->
            <div style="clear: both;"></div>
        </div>
        <!-- End #subnav -->
    </div>
    <!-- End #header -->

    <div class="container_12">
        <div class="bottom-spacing">
            <!-- Button -->
            <div class="float-left">
                <a href="addsp.php" class="button">
                    <span>Thêm sản phẩm <img src="/shopda/templates/images/plus-small.gif" alt="Thêm tin"></span>
                </a>
            </div>
            <div class="clear"></div>
        </div>
    <?php
        if(isset($_GET['msg'])){
            $tb = $_GET['msg'];
            echo '<strong>'.$tb.'</strong>';
        }

    ?>

        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <?php
                    $rowcount = ad_rowcount;
                    if(isset($_POST['hienthi']))
                    {
                        header ("LOCATION:index.php");
                    }else
                    {
                        if(isset($_POST['tim']))
                        {
                            $key = $_POST['search'];
                            $selectDM = $_POST['selectDM'];
                            if($selectDM == 0)
                            {
                                $query = "SELECT sanpham.*,loaisp.loaisp AS tenloaisp FROM sanpham INNER JOIN loaisp
                                        ON sanpham.id_loaisp = loaisp.id_loaisp WHERE tensp LIKE '%$key%' ";    
                            }else
                            {
                                $query = "SELECT sanpham.*,loaisp.loaisp AS tenloaisp FROM sanpham INNER JOIN loaisp
                                        ON sanpham.id_loaisp = loaisp.id_loaisp WHERE tensp LIKE '%$key%' && loaisp.id_loaisp = '$selectDM'";
                            }
                            
                        }else
                        {
                            if(isset($_POST['selectDM']))
                            {
                                $selectDM = $_POST['selectDM'];
                                if($selectDM != 0)
                                {
                                    $query = "SELECT sanpham.*,loaisp.loaisp AS tenloaisp FROM sanpham INNER JOIN loaisp
                                        ON sanpham.id_loaisp = loaisp.id_loaisp WHERE loaisp.id_loaisp = '$selectDM'";    
                                }
                                else
                                {
                                    header ("LOCATION:index.php");
                                }
                            }else
                            {
                                if(isset($_GET['id_loaisp']))
                                {
                                    $selectDM = $_GET['id_loaisp'];
                                    $query = "SELECT sanpham.*,loaisp.loaisp AS tenloaisp FROM sanpham INNER JOIN loaisp
                                        ON sanpham.id_loaisp = loaisp.id_loaisp WHERE loaisp.id_loaisp = '$selectDM'";    
                                }
                                else
                                {
                                    $query = "SELECT sanpham.*,loaisp.loaisp AS tenloaisp FROM sanpham INNER JOIN loaisp
                                        ON sanpham.id_loaisp = loaisp.id_loaisp";    
                                }
                                
                            }
                            
                        }

                    }
                    
                ?>
                <!--nchange="this.form.submit()": tu dongload lai form -->
                <form action="index.php" method="POST" enctype="multipart/form-data" id="frmsearch">
                    Danh mục:
                    <select id="selectDM" name="selectDM" onchange="this.form.submit()" style="width: 160px">
						<option value="0">Tất cả danh mục</option>
						<?php
							$query_loaisp = "SELECT * FROM loaisp";
							$result_loaisp = $mysqli->query($query_loaisp);
							while($row_loaisp = mysqli_fetch_assoc($result_loaisp)){
								$id_loaisp = $row_loaisp['id_loaisp'];
								$loaisp = $row_loaisp['loaisp'];
								if(isset($selectDM)){
									if($selectDM == $id_loaisp)
									{
										$select = "selected = 'selected'";
									}else{
										$select = '';
									}
								}
								else{
									$select = '';
								}
							
						?>
						<option   value="<?php echo $id_loaisp;?>"<?php echo $select;?>><?php echo $loaisp;?></option>
						<?php } ?>
					</select> Tìm kiếm:
                    <input type="text" name="search" value="" placeholder="Từ khóa tin tức" />
                    <input type="submit" name="tim" value="Tìm kiếm" />
                    <input type="submit" name="hienthi" value="Hiển thị tất cả" />
                </form>
                <h2><span>Danh sách tin</span></h2>

                <div class="module-table-body">
                    <form action="delsp.php" method="POST" enctype="multipart/form-data" id="frmtbl">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:4%; text-align: center;">STT</th>
                                    <th>Tên</th>
                                    <th style="width:20%">Danh mục</th>
                                    <th style="width:16%; text-align: center;">Hình ảnh</th>
                                    <th style="width:8%; text-align: center;">Chức năng</th>
                                    <th style="width:6%; text-align: center;">
                                        <a href=""><input onclick="return confirm('Are you sure you want to delete?');" type="submit" name="delete" value="Xóa" /><img src="/shopda/templates/images/bin.gif" width="16" height="16" alt="delete" /></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $result = $mysqli->query($query);
                                    $tongdong = mysqli_num_rows($result);
                                    $tongtrang = ceil($tongdong/$rowcount);
                                    if(isset($_GET['page'])){
                                        $currentpage = $_GET['page'];    
                                    }
                                    else
                                    {
                                        $currentpage = 1;
                                    }
                                    $offset = ($currentpage -1) * $rowcount; 
                                    $query = $query.' ORDER BY id_sp DESC LIMIT '.$offset.','.$rowcount  ;
                                    $result = $mysqli->query($query);
                                    $i = $offset + 1;
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        $id_sp = $row['id_sp'];
                                        $tenloaisp = $row['tenloaisp'];
                                        $tensp = $row['tensp'];
                                        $hinhanh = $row['hinhanh'];
                                        if ($hinhanh != NULL){
                                            $url = '/'.ROOT_DIR."/files/{$hinhanh}";
                                        }
                                    
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $i;?></td>
                                    <td><a href=""><?php echo $tensp;?></a></td>
                                    <td><?php echo $tenloaisp;?></td>
                                    <td align="center">
                                        <?php 
                                            if($hinhanh != NULL){

                                        ?>
                                        <img src="<?php echo $url;?>" />
                                        <?php
                                             }else{
                                               echo "<strong>Không có hình</strong>";
                                            } 
                                        ?>
                                    </td>
                                    <td align="center">

                                        <a href="editsp.php?id_sp=<?php echo $id_sp;?>">Sửa <img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" name="<?php echo $id_sp;?>" value="<?php echo $id_sp;?>" />
                                    </td>
                                </tr>
                                <?php
                                    $i++;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->

            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <?php
                        if(isset($selectDM))
                        {
                            $link = "&id_loaisp=$selectDM";
                        }else
                        {
                            $link = '';
                        }
                        for($i = 1; $i<=$tongtrang; $i++){
                            if($i != $currentpage)
                            {
                    ?>
                        <a href="index.php?page=<?php echo $i;?><?php echo $link;?>" class='current'><?php echo $i;?></a>
                     <span>|</span>
                    <?php     
                            }
                            else
                            {
                    ?>
                        <span style="color:#C22B3B;font-weight:bold;"><?php echo $i;?></span>
                        <span>|</span>
                    <?php
                            }
                        }
                    ?>
                    
                    

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>