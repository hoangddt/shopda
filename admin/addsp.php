﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/config.php';?>
<style>
    .mau{
        color:#ED5565;
    };
</style>
<!-- Form elements -->    
<div class="grid_12">

    <div class="module">
         <h2><span>Thêm sản phẩm</span></h2>
            
         <div class="module-body">
            <?php
                if(isset($_POST['them'])){
                    $tensp= $mysqli->real_escape_string($_POST['tensp']);
                    //$tentin= $_POST['tentin'];
                    $id_sp= $_POST['danhmuc'];
                    $mota= $_POST['mota'];
                    $chitiet= $_POST['chitiet'];
                    $gia = $_POST['giasp'];
                    $tenhinh= $_FILES['hinhanh']['name'];
                    if($tenhinh == NULL){
                        $sql_st1= "INSERT INTO sanpham(id_loaisp, tensp, mota, chitiet, giasp) 
                        VALUES('$id_sp', '$tensp', '$mota', '$chitiet', '$gia')"; 
                        $result1= $mysqli->query($sql_st1);
                        if($result1){
                            header("LOCATION: index.php?msg=Thêm sản phẩm thành công");
                            exit();
                        }else{
                            echo '<strong>Có lỗi khi thêm sản phẩm</strong>';
                        }
                    }else{
                        $chuoi= explode(".", $tenhinh);
                        $count= count($chuoi);
                        $duoifile= $chuoi[$count-1];
                        $time= time();
                        unset($chuoi[$count-1]);
                        $chuoiMoi= '';
                        foreach($chuoi as $key=>$value){
                            if($key == 0){
                                $chuoiMoi= $value;
                            }else{
                                $chuoiMoi= $chuoiMoi.$value;
                            }
                        }
                        $tenhinh= $chuoiMoi.'_'.$time.'.'.$duoifile;
                        $tmp_name= $_FILES['hinhanh']['tmp_name'];
                        $path_upload= $_SERVER['DOCUMENT_ROOT'].'/'.ROOT_DIR.'/files/'.$tenhinh;                     
                        $ketqua= move_uploaded_file($tmp_name, $path_upload);
                        if($ketqua > 0){
                            $sql_st2= "INSERT INTO sanpham(id_loaisp, tensp, mota, chitiet, giasp, hinhanh) 
                                     VALUES('$id_sp', '$tensp', '$mota', '$chitiet','$gia','$tenhinh')";
                            $result2= $mysqli->query($sql_st2);
                            if($result2 >= 1){
                                header("LOCATION: index.php?msg=Thêm sản phẩm thành công");
                                exit();
                            }else{
                                echo '<strong>Có lỗi khi thêm sản phẩm</strong>';
                            }
                        }else{
                            echo '<strong>Có lỗi khi upload hình</strong>';
                        }
                    }
                }
            ?>
<style>
    .error{
        color: red;
    }
</style>
<script>
    $(document).ready(function(){
        $('#themtin').validate({
            rules:{
                tentin:{
                    required: true,
                }
            },
            
            messages:{
                tentin:{
                    required: "Vui lòng nhập tên tin",
                }
            },
        });
    });
</script>
            <form id="themsp" action="" method="post" enctype="multipart/form-data">
                <p>
                    <label>Tên sản phẩm</label>
                    <input type="text" name="tensp" value="" class="input-medium" />
                </p>
                <p>
                    <label>Danh mục sản phẩm</label>
                    <select  name="danhmuc" class="input-short">
                        <?php
                            $query= "SELECT * FROM loaisp";
                            $result= $mysqli->query($query);
                            while($row= mysqli_fetch_assoc($result)){
                                $id_loaisp= $row['id_loaisp'];
                                $loaisp= $row['loaisp'];
                        ?>
                        <option value="<?php echo $id_loaisp;?>"><?php echo $loaisp;?></option>
                        <?php
                            }
                        ?>
                    </select>
                </p>
                <p>
                    <label>Hình ảnh</label>
                    <input type="file"  name="hinhanh" value="" />
                </p>
                <p>
                    <label>Mô tả</label>
                    <textarea name="mota" value="" rows="7" cols="90" class="input-medium"></textarea>
                </p>
                <p style = "width:50%;">
                    <label>Chi tiết</label>
                    <textarea  name="chitiet" value="" rows="7" cols="90" class="input-long ckeditor"></textarea>
                </p>
                <p>
                    <label>Giá sản phẩm</label>
                    <input type="text" name="giasp" value="" class="input-medium" />
                </p>
                <fieldset>
                    <input class="submit-green" name="them" type="submit" value="Thêm" /> 
                    <input class="submit-gray" name="reset" type="reset" value="Nhập lại" />
                </fieldset>
                
            </form>
         </div> <!-- End .module-body -->

    </div>  <!-- End .module -->
    <div style="clear:both;"></div>
</div> <!-- End .grid_12 -->
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>