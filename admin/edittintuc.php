﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php
    if(isset($_GET['id_qc'])){
        $idqc = $_GET['id_qc'];
        $sql = "SELECT * FROM quangcao WHERE id_qc = '$idqc'";
        $result = $mysqli->query($sql);
        $arr = mysqli_fetch_assoc($result);
        $ten = $arr['tenqc'];
        $link = $arr['link'];
        $hinh = $arr['hinh'];
    }
?>
<div class="grid_12">
    <div class="module">
        <h2><span>Sửa quảng cáo</span></h2>
        <?php
            if(isset($_POST['sua']))
            {
                $tenqc = $_POST['ten'];
                $linkqc = $_POST['link'];
                $tenhinh = $_FILES['hinhanh']['name'];
                if($tenhinh == NULL)
                {
                    $sql_up1 = "UPDATE quangcao SET tenqc = '$tenqc', link = '$linkqc' WHERE id_qc = '$idqc'";
                }
                else
                {
                    $chuoi = explode('.', $tenhinh);
                    $duoifile = end($chuoi);
                    $ha = $tensp.'-'.time().'.'.$duoifile;
                    $file = $_FILES['hinhanh']['tmp_name'];
                    $name = $_SERVER['DOCUMENT_ROOT'].'/files/'.$ha;
                    $kq = move_uploaded_file($file,$name);
                    if($kq > 0)
                    {
                        $sql_up1 = "UPDATE quangcao SET tenqc = '$tenqc', link = '$linkqc', hinh = '$ha' WHERE id_qc = '$idqc'";
                    }
                    else
                    {
                        echo "<strong>Có lỗi xảy ra</strong>";
                    }
                }
                $result_up1 = $mysqli->query($sql_up1);
                if($result_up1)
                {
                    header("LOCATION:quangcao.php?msg=Sửa quảng cáo thành công");
                }
                else
                {
                echo "<strong>Có lỗi xảy ra</strong>";
                }

            }
        ?>
        <div class="module-body">
            <form action="" method="POST" enctype="multipart/form-data" id="editAdv">
                <p>
                    <label>Tên (*)</label>
                    <input type="text" name="ten" value="<?php echo $ten;?>" class="input-medium" />
                </p>
                <p>
                    <label>Link (*)</label>
                    <input type="text" name="link" value="<?php echo $link;?>" class="input-medium" />
                </p>
                <p>
                    <img src="/shopda/files/<?php echo $hinh;?>" width="200px" height="200px" />
                </p>

                <p>
                    <label>Hình ảnh</label>
                    <input type="file" name="hinhanh" value="" />
                </p>
                <fieldset>
                    <input class="submit-green" name="sua" type="submit" value="Sửa" />
                    <input class="submit-gray" name="reset" type="reset" value="Nhập lại" />
                </fieldset>
            </form>
        </div>
        <!-- End .module-body -->

    </div>
    <!-- End .module -->
    <div style="clear:both;"></div>
</div>
<!-- End .grid_12 -->
<script>
    $(document).ready(function() {
        $('#editAdv').validate({
            rules: {
                ten: {
                    required: true,
                },
                link: {
                    required: true,
                },
            },
            messages: {
                ten: {
                    required: "<strong>Vui lòng nhập tên quảng cáo</strong>",
                },
                link: {
                    required: "<strong>Vui lòng nhập link quảng cáo</strong>",
                },
            },
        });
    });
</script>
</div>
<div style="clear:both;"></div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>