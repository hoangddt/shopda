﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/checkuser.php';?>
<?php
	if(isset($_GET['msg'])){
		$msg = $_GET['msg'];
		
	}
?>
<?php
	$id_dh = $_GET['id_dh'];
	if(isset($_POST['duyet'])){
		$sql = "UPDATE dathang SET tinhtrang = 1 WHERE id_dh = $id_dh";
		$result=$mysqli->query($sql);
		if($result){
			header("LOCATION:donhang.php?msg=Duyệt đơn hàng thành công");
		}
		else
		{
			echo "Lỗi khi duyệt";
		}
	}
	
	if(isset($_POST['xoa'])){
		$sql_xoa = "DELETE FROM dathang WHERE id_dh = $id_dh";
		$result_xoa = $mysqli->query($sql_xoa);
		if($result_xoa){
			header("LOCATION:donhang.php?msg=Hủy đơn hàng thành công");
		}
		else
		{
			echo "Lỗi khi xóa";
		}
		
	}
?>
    <div class="container_12">
       <?php
			if(isset($msg)){
				echo "<p style='color:red;'>$msg</p>";
			}
		?>
        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <h2><span>Danh sách tin</span></h2>
				
                <div class="module-table-body">
                    <form action="" method="POST" enctype="multipart/form-data" id="frmtbl">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:4%; text-align: center;">STT</th>
                                    <th>Tên sản phẩm</th>
                                    <th style="width:20%">Số lượng</th>
                                    <th style="width:12%">Đơn giá</th>
                                    <th style="width:12%">Thành tiền</th>
                                    <th style="width:12%">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
									$i = 1;
                                    if(isset($_GET['id_dh'])){
										$id_dh = $_GET['id_dh'];
										$sql = "SELECT * FROM chitietdathang INNER JOIN sanpham ON sanpham.id_sp = chitietdathang.id_sp WHERE id_dh = $id_dh";
										
										$result = $mysqli->query($sql);
										
										while($arr = mysqli_fetch_assoc($result)){
											$id_sp = $arr['id_sp'];
											$ten = $arr['tensp'];
											$soluong = $arr['soluong'];
											$dongia = $arr['giasp'];
											$tt = $arr['thanhtien'];
										
                                        
                                    
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $i;?></td>
                                    <td><a href=""><?php echo $ten;?></a></td>
                                    <td><?php echo $soluong;?></td>
                                    <td><?php echo $dongia;?></td>
									<td><?php echo $tt;?></td>
                                    
                                    <td align="center">

                                        <a href="xoadonhang.php?id_dh=<?php echo $id_dh;?>&id_sp=<?php echo $id_sp;?>">Xóa<img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                    </td>
                                </tr>
                               <?php
                                    $i++;
                                    }
								}
										
									
                               ?>
							   <tr>
									<td colspan="6" align="center">
										<input type="submit" value="Duyệt" name="duyet" />
										<input type="submit" value="Xóa" name="xoa" />
									</td>
							   </tr>
							   
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->

            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <a href="indexNews.php?page=1" class='current'>1</a>
                    <span>|</span>
                    <a href="indexNews.php?page=2">2</a>
                    <span>|</span>
                    <a href="indexNews.php?page=3">3</a>
                    <span>|</span>
                    <a href="indexNews.php?page=4">4</a>
                    <span>|</span>
                    <a href="indexNews.php?page=5">5</a>
                    <span>|</span>
                    <a href="indexNews.php?page=6">6</a>
                    <span>|</span>

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
    <!-- Footer -->
   
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>