﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/checkuser.php';?>


    <div class="container_12">
       
        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <h2><span>Danh sách tin</span></h2>
		
                <div class="module-table-body">
                    <form action="" method="POST" enctype="multipart/form-data" id="frmtbl">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:4%; text-align: center;">STT</th>
                                    <th>Tên khách hàng</th>
                                    <th style="width:20%">Địa chỉ</th>
                                    <th style="width:12%">Số điện thoại</th>
                                    <th style="width:12%">Ngày đặt</th>
                                    <th style="width:12%">Tổng tiền</th>
									<th style="width:12%">Chi tiết</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    $sql = "SELECT * FROM dathang WHERE tinhtrang = 0";
                                    $result = $mysqli->query($sql);
                                    while($arr = mysqli_fetch_assoc($result)){
                                        $id_dh = $arr['id_dh'];
                                        $tenkh = $arr['tenkh'];
                                        $sdt = $arr['sdt'];
                                        $diachi = $arr['diachi'];
										$tongtien = $arr['tongtien'];
										$ngaydat = $arr['ngaydat'];
                                        $tachngay = explode('-', $ngaydat);
                                        $ngaydat = $tachngay[2].'-'.$tachngay[1].'-'.$tachngay[0];
                                        
                                    
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $i;?></td>
                                    <td><a href=""><?php echo $tenkh;?></a></td>
                                    <td><?php echo $diachi;?></td>
                                    <td><?php echo $sdt;?></td>
									<td><?php echo $ngaydat;?></td>
									<td><?php echo $tongtien;?></td>
                                    
                                    <td align="center">

                                        <a href="chitietdh.php?id_dh=<?php echo $id_dh;?>">Xem<img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                    </td>
                                </tr>
                               <?php
                                    $i++;
                                    }
                               ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->

            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <a href="indexNews.php?page=1" class='current'>1</a>
                    <span>|</span>
                    <a href="indexNews.php?page=2">2</a>
                    <span>|</span>
                    <a href="indexNews.php?page=3">3</a>
                    <span>|</span>
                    <a href="indexNews.php?page=4">4</a>
                    <span>|</span>
                    <a href="indexNews.php?page=5">5</a>
                    <span>|</span>
                    <a href="indexNews.php?page=6">6</a>
                    <span>|</span>

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
    <!-- Footer -->
   
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>