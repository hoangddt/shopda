﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/config.php';?>

<?php
    if(isset($_GET['id_sp'])){
        $id = $_GET['id_sp'];
        $sql = "SELECT * FROM sanpham WHERE id_sp = '$id'";
        $result = $mysqli->query($sql);
        $arr = mysqli_fetch_assoc($result);
        $ten = $arr['tensp'];
        $danhmuc = $arr['id_loaisp'];
        $mota = $arr['mota'];
        $chitiet = $arr['chitiet'];
        $giasp = $arr['giasp'];
        $ha = $arr['hinhanh'];

    }
?>
    <div class="container_12">
        <!-- Form elements -->
        <div class="grid_12">
            <div class="module">
                <h2><span>Sửa sản phẩm</span></h2>
<?php
    if(isset($_POST['sua']))
    {
        $tensp = $_POST['tensp'];
        $danhmuc = $_POST['danhmuc'];
        $chitiet = $_POST['chitiet'];
        $mota = $_POST['mota'];
        $giasp = $_POST['giamoi'];
        $tenhinh = $_FILES['hinhanh']['name'];
        if($tenhinh == NULL)
        {
            $sql_up1 = "UPDATE sanpham SET tensp = '$tensp', id_loaisp = '$danhmuc', mota = '$mota', chitiet = '$chitiet',
                     giasp = '$giasp' WHERE id_sp = '$id'";
        }
        else
        {
            $chuoi = explode('.', $tenhinh);
            $duoifile = end($chuoi);
            $ha = $tensp.'-'.time().$duoifile;
            $file = $_FILES['hinhanh']['tmp_name'];
            $name = $_SERVER['DOCUMENT_ROOT'].'/'.ROOT_DIR.'/files/'.$ha;
            $kq = move_uploaded_file($file,$name);
            if($kq > 0)
            {
                $sql_up1 = "UPDATE sanpham SET tensp = '$tensp', id_loaisp = '$danhmuc', mota = '$mota', chitiet = '$chitiet',
                     giasp = '$giasp', hinhanh = '$ha'  WHERE id_sp = '$id'";
            }
            else
            {
                echo "<strong>Có lỗi xảy ra</strong>";
            }
        }
        $result_up1 = $mysqli->query($sql_up1);
        if($result_up1)
        {
            header("LOCATION:index.php?msg=Sửa sản phẩm thành công");
        }
        else
        {
        echo "<strong>Có lỗi xảy ra</strong>";
        }

    }
?>
                <div class="module-body">
                    <form action="" method="POST" enctype="multipart/form-data" id="frmedit">
                        <p>
                            <label>Tên sản phẩm(*)</label>
                            <input type="text" name="tensp" value="<?php echo $ten?>" class="input-medium" />
                        </p>
                        <p>
                            <label>Danh mục sản phẩm</label>
                            <select name="danhmuc" class="input-short">
                                <option value="0">Tất cả danh mục</option>
                                <?php
                                    $query_loaisp = "SELECT * FROM loaisp";
                                    $result_loaisp = $mysqli->query($query_loaisp);
                                    while($row_loaisp = mysqli_fetch_assoc($result_loaisp)){
                                        $id_loaisp = $row_loaisp['id_loaisp'];
                                        $loaisp = $row_loaisp['loaisp'];
                                        if(isset($danhmuc)){
                                            if($danhmuc == $id_loaisp)
                                            {
                                                $select = "selected = 'selected'";
                                            }else{
                                                $select = '';
                                            }
                                        }
                                        else{
                                            $select = '';
                                        }
                                    
                                ?>
                                <option   value="<?php echo $id_loaisp;?>"<?php echo $select;?>><?php echo $loaisp;?></option>
                                <?php } ?>
                            </select>
                        </p>
                        <p>
                            <label>Hình ảnh</label>
                            <input type="file" name="hinhanh" />
                            <img src="/files/<?php echo $ha;?>" />
                        </p>

                        <p>
                            <label>Mô tả(*)</label>
                            <textarea name="mota" rows="7" cols="90" class="input-medium"><?php echo $mota;?></textarea>
                        </p>
                        <p>
                            <label>Chi tiết</label>
                            <textarea name="chitiet" value="" rows="7" cols="90" class="input-long ckeditor"><?php echo $chitiet;?></textarea>
                        </p>
                        <p>
                            <label>Giá mới</label>
                            <input type="text" name="giamoi" value="<?php echo $giasp?>" class="input-medium" />
                        </p>
                        <fieldset>
                            <input class="submit-green" name="sua" type="submit" value="Sửa" />
                            <input class="submit-gray" name="reset" type="reset" value="Nhập lại" />
                        </fieldset>
                    </form>
                </div>
                <!-- End .module-body -->

            </div>
            <!-- End .module -->
            <div style="clear:both;"></div>
        </div>
        <!-- End .grid_12 -->
        <script>
            $(document).ready(function() {
                $('#frmedit').validate({
                    rules: {
                        tentin: {
                            required: true,
                        },
                        mota: {
                            required: true,
                        },
                        chitiet: {
                            required: true,
                        },
                    },
                    messages: {
                        tentin: {
                            required: "<strong>Vui lòng nhập tên tin</strong>",
                        },
                        mota: {
                            required: "<strong>Vui lòng nhập mô tả</strong>",
                        },
                        chitiet: {
                            required: "<strong>Vui lòng nhập chi tiết</strong>",
                        },
                    },
                });
            });
        </script>
    </div>
    <div style="clear:both;"></div>
    <?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>