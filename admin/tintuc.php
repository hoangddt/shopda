﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/checkuser.php';?>
<?php
    if(isset($_GET['msg'])){
        $msg = $_GET['msg'];
        echo $msg;
    }
?>

    <div class="container_12">
        <div class="bottom-spacing">
            <!-- Button -->
            <div class="float-left">
                <a href="addtintuc.php" class="button">
                    <span>Thêm tin <img src="/shopda/templates/images/plus-small.gif" alt="Thêm tin"></span>
                </a>
            </div>
            <div class="clear"></div>
        </div>


        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <h2><span>Danh sách tin</span></h2>

                <div class="module-table-body">
                    <form action="deltintuc.php" method="POST" enctype="multipart/form-data" id="frmtbl">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:4%; text-align: center;">STT</th>
                                    <th>Tên tin</th>
                                    <th style="width:20%">Mô tả</th>
                                    <th style="width:12%">Ngày đăng</th>
                                    <th style="width:12%">Hình ảnh</th>
                                    <th style="width:12%">Sửa</th>
                                    <th style="width:6%; text-align: center;">
                                        <a href="" onclick="return confirm('Are you sure you want to delete?');"><input type="submit" name="delete" value="Xóa" /><img src="/shopda/templates/images/bin.gif" width="16" height="16" alt="delete" /></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    $sql = "SELECT * FROM tintuc ORDER BY id_tt DESC";
                                    $result = $mysqli->query($sql);
                                    while($arr = mysqli_fetch_assoc($result)){
                                        $id = $arr['id_tt'];
                                        $tentin = $arr['tentt'];
                                        $mota = $arr['mota'];
                                        $ngaydang = $arr['ngaydang'];
                                        $tachngay = explode('-', $ngaydang);
                                        $ngaydang = $tachngay[2].'-'.$tachngay[1].'-'.$tachngay[0];
                                        $hinhanh = $arr['hinhanh'];
                                        if ($hinhanh != NULL){
                                            $url = "/files/{$hinhanh}";
                                        }
                                    
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $id;?></td>
                                    <td><a href=""><?php echo $tentin;?></a></td>
                                    <td><?php echo $mota;?></td>
                                    <td><?php echo $ngaydang;?></td>
                                    <td>
                                        <?php 
                                                if($hinhanh != NULL){

                                            ?>
                                            <img src="<?php echo $url;?>" />
                                            <?php
                                                 }else{
                                                   echo "<strong>Không có hình</strong>";
                                                } 
                                        ?>
                                    </td>
                                    <td align="center">

                                        <a href="edittintuc.php?id=<?php echo $id;?>">Sửa <img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" name="<?php echo $id;?>" value="<?php echo $id;?>" />
                                    </td>
                                </tr>
                               <?php
                                    $i++;
                                    }
                               ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->

            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <a href="indexNews.php?page=1" class='current'>1</a>
                    <span>|</span>
                    <a href="indexNews.php?page=2">2</a>
                    <span>|</span>
                    <a href="indexNews.php?page=3">3</a>
                    <span>|</span>
                    <a href="indexNews.php?page=4">4</a>
                    <span>|</span>
                    <a href="indexNews.php?page=5">5</a>
                    <span>|</span>
                    <a href="indexNews.php?page=6">6</a>
                    <span>|</span>

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
    <!-- Footer -->
   
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>