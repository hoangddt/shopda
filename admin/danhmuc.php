﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>

  <?php
        if(isset($_GET['msg'])){
            $tb = $_GET['msg'];
            echo '<strong>'.$tb.'</strong>';
        }
    ?>
    <div class="container_12">

        <div class="bottom-spacing">
            <!-- Button -->
            <div class="float-left">
                <a href="adddanhmuc.php" class="button">
                    <span>Thêm tên danh mục tin <img src="/shopda/templates/images/plus-small.gif" alt="Thêm tin"></span>
                </a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <h2><span>Danh sách danh mục tin</span></h2>
                <div class="module-table-body">
                    <form action="">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:10%; text-align: center;">STT</th>
                                    <th>Tên Danh Mục Tin</th>
                                    <th style="width:30%; text-align: center;">Chức năng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $query_dm = "SELECT * FROM loaisp ORDER BY id_loaisp DESC";
                                    $result_dm = $mysqli->query($query_dm);
                                    while($arr_dm = mysqli_fetch_assoc($result_dm)){
                                        $id_loaisp = $arr_dm['id_loaisp'];
                                        $loaisp = $arr_dm['loaisp'];
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $id_loaisp;?></td>
                                    <td><?php echo $loaisp;?></td>
                                    <td align="center">
                                        <a href="editdanhmuc.php?id_loaisp=<?php echo $id_loaisp;?>">Sửa <img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                        <a href="deldanhmuc.php?id_loaisp=<?php echo $id_loaisp;?>" onclick="return confirm('Are you sure you want to delete this item?');">Xóa <img src="/shopda/templates/images/bin.gif" width="16" height="16" alt="delete" /></a>
                                    </td>
                                </tr>
                                <?php
                                    }
                                ?>
                                
                                
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->
            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <a href="indexCat.php?page=1" class='current'>1</a>
                    <span>|</span>

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
    <!-- Footer -->
    <div id="footer">
        <div class="container_12">
            <div class="grid_12">
                <p>Copyright &copy; 2013. <a href="http://vinatab.net/edu" title="Đã học là làm được">QuangPhu9</a></p>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <!-- End #footer -->
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>