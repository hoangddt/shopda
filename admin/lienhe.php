﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<div class="container_12">
    <div class="grid_12">
        <!-- Example table -->
        <div class="module">
            <h2><span>Danh sách liên hệ</span></h2>

            <div class="module-table-body">
            
                <form action="" method="POST" enctype="multipart/form-data" id="frmcont">
                    <table id="myTable" class="tablesorter">
                        <thead>
                            <tr>
                                <th style="width:10%; text-align: center;">STT</th>
                                <th style="width:25%">Họ và tên</th>
                                <th style="width:25%">Email</th>
                                <th style="width:25%">Nội dung</th>
                                <th style="width:11%; text-align: center;">
                                    <a href="" onclick="return confirm('Are you sure you want to delete?');"><input type="submit" name="delete" value="Xóa" /><img src="/templates/images/bin.gif" width="16" height="16" alt="delete" /></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $sql = "SELECT * FROM lienhe";
                            $result = $mysqli->query($sql);
                            while($arr = mysqli_fetch_assoc($result)){
                                $id_lh = $arr['id_lh'];
                                $hoten = $arr['hoten'];
                                $email = $arr['email'];
                                $noidung = $arr['noidung'];
                        ?>
                            <tr>
                                <td class="align-center"><?php echo $id_lh;?></td>
                                <td><a href="detailCont.php?id_contact=17"><?php echo $hoten;?></a></td>
                                <td><?php echo $email;?></td>
                                <td><?php echo $noidung;?></td>
                                <td align="center">
                                    <input type="checkbox" name="check[]" value="17" />
                                </td>
                            </tr>
                        <?php
                            }
                        ?>     
                        </tbody>
                    </table>
                </form>
            </div>
            <!-- End .module-table-body -->
        </div>
        <!-- End .module -->
        <div class="pagination">
            <div class="numbers">
                <span>Trang:</span>
                <a href="indexCont.php?page=1" class='current'>1</a>
                <span>|</span>

            </div>
            <div style="clear: both;"></div>
        </div>

    </div>
    <!-- End .grid_12 -->
</div>
<div style="clear:both;"></div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>