﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<style>
    .mau{
        color:#ED5565;
    };
</style>
<!-- Form elements -->    
<div class="grid_12">

    <div class="module">
         <h2><span>Thêm tin tức</span></h2>
            
         <div class="module-body">
            <?php
                if(isset($_POST['them'])){
                    $tentt= $mysqli->real_escape_string($_POST['tentt']);
                    //$tentin= $_POST['tentin'];
                    $date=getdate();
                    $ngaydang=$date['year'].'/'.$date['mon'].'/'.$date['mday'];
                    $mota= $_POST['mota'];
                    $chitiet= $_POST['chitiet'];
                    $tenhinh= $_FILES['hinhanh']['name'];
                    if($tenhinh == NULL){
                        $sql_st1= "INSERT INTO tintuc(id_tt, tentt, mota, noidung, ngaydang) 
                        VALUES('$id_tt', '$tentt', '$mota', '$chitiet', '$ngaydang')"; 
                        $result1= $mysqli->query($sql_st1);
                        if($result1){
                            header("LOCATION: tintuc.php?msg=Thêm tin thành công");
                            exit();
                        }else{
                            echo '<strong>Có lỗi khi thêm tin</strong>';
                        }
                    }else{
                        $chuoi= explode(".", $tenhinh);
                        $count= count($chuoi);
                        $duoifile= $chuoi[$count-1];
                        $time= time();
                        unset($chuoi[$count-1]);
                        $chuoiMoi= '';
                        foreach($chuoi as $key=>$value){
                            if($key == 0){
                                $chuoiMoi= $value;
                            }else{
                                $chuoiMoi= $chuoiMoi.$value;
                            }
                        }
                        $tenhinh= $chuoiMoi.'_'.$time.'.'.$duoifile;
                        $tmp_name= $_FILES['hinhanh']['tmp_name'];
                        $path_upload= $_SERVER['DOCUMENT_ROOT'].'/files/'.$tenhinh;                     
                        $ketqua= move_uploaded_file($tmp_name, $path_upload);
                        if($ketqua > 0){
                            $sql_st2= "INSERT INTO tintuc(id_tt, tentt, mota, noidung, ngaydang, hinhanh) 
                        VALUES('$id_tt', '$tentt', '$mota', '$chitiet', '$ngaydang', '$tenhinh')"; 
                            $result2= $mysqli->query($sql_st2);
                            if($result2 >= 1){
                                header("LOCATION: tintuc.php?msg=Thêm tin thành công");
                                exit();
                            }else{
                                echo '<strong>Có lỗi khi thêm sản phẩm</strong>';
                            }
                        }else{
                            echo '<strong>Có lỗi khi upload hình</strong>';
                        }
                    }
                }
            ?>
<style>
    .error{
        color: red;
    }
</style>
<script>
    $(document).ready(function(){
        $('#themtin').validate({
            rules:{
                tentin:{
                    required: true,
                }
            },
            
            messages:{
                tentin:{
                    required: "Vui lòng nhập tên tin",
                }
            },
        });
    });
</script>
            <form id="themsp" action="" method="post" enctype="multipart/form-data">
                <p>
                    <label>Tên tin tức</label>
                    <input type="text" name="tentt" value="" class="input-medium" />
                </p>
                <p>
                    <label>Hình ảnh</label>
                    <input type="file"  name="hinhanh" value="" />
                </p>
                <p>
                    <label>Mô tả</label>
                    <textarea name="mota" value="" rows="7" cols="90" class="input-medium"></textarea>
                </p>
                <p style = "width:50%;">
                    <label>Chi tiết</label>
                    <textarea  name="chitiet" value="" rows="7" cols="90" class="input-long ckeditor"></textarea>
                </p>                <fieldset>
                    <input class="submit-green" name="them" type="submit" value="Thêm" /> 
                    <input class="submit-gray" name="reset" type="reset" value="Nhập lại" />
                </fieldset>
                
            </form>
         </div> <!-- End .module-body -->

    </div>  <!-- End .module -->
    <div style="clear:both;"></div>
</div> <!-- End .grid_12 -->
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>