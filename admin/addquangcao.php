﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>


   <div class="container_12">
    <!-- Form elements -->
    <div class="grid_12">

        <div class="module">
            <h2><span>Thêm quảng cáo</span></h2>
            <?php
                if(isset($_POST['them'])){
                    $tenqc= $mysqli->real_escape_string($_POST['tenquangcao']);;
                    $link= $_POST['link'];
                    $tenhinh= $_FILES['hinhanh']['name'];
                    if($tenhinh == NULL){
                        $sql_st1= "INSERT INTO quangcao(tenqc, link)
                        VALUES('$tenqc','$link')";
                        $result1= $mysqli->query($sql_st1);
                        if($result1){
                            header("LOCATION: quangcao.php?msg=Thêm quảng cáo thành công");
                            exit();
                        }else{
                            echo '<strong>Có lỗi khi thêm quảng cáo</strong>';
                        }
                    }else{
                        $chuoi= explode(".", $tenhinh);
                        $count= count($chuoi);
                        $duoifile= $chuoi[$count-1];
                        $time= time();
                        unset($chuoi[$count-1]);
                        $chuoiMoi= '';
                        foreach($chuoi as $key=>$value){
                            if($key == 0){
                                $chuoiMoi= $value;
                            }else{
                                $chuoiMoi= $chuoiMoi.$value;
                            }
                        }
                        $tenhinh= $chuoiMoi.'_'.$time.'.'.$duoifile;
                        $tmp_name= $_FILES['hinhanh']['tmp_name'];
                        $path_upload= $_SERVER['DOCUMENT_ROOT'].'/files/'.$tenhinh;                     
                        $ketqua= move_uploaded_file($tmp_name, $path_upload);
                        if($ketqua){
                            $sql_st2= "INSERT INTO quangcao(tenqc, link, hinh)
                        VALUES('$tenqc','$link', '$tenhinh')";
                            $result2= $mysqli->query($sql_st2);
                            if($result2){
                                header("LOCATION: quangcao.php?msg=Thêm quảng cáo thành công");
                                exit();
                            }else{
                                echo '<strong>Có lỗi khi thêm quảng cáo</strong>';
                            }
                        }else{
                            echo '<strong>Có lỗi khi upload hình</strong>';
                        }
                    }
                }

            ?>
            <div class="module-body">
                <form action="" method="POST" enctype="multipart/form-data" id="addAdv">
                    <p>
                        <label>Tên quảng cáo(*)</label>
                        <input type="text" name="tenquangcao" value="" class="input-medium" />
                    </p>
                    <p>
                        <label>Hình ảnh</label>
                        <input type="file" name="hinhanh" value="" />
                    </p>
                    <p>
                        <label>Link quảng cáo(*)</label>
                        <input type="text" name="link" value="" class="input-medium" />
                    </p>
                    <fieldset>
                        <input class="submit-green" name="them" type="submit" value="Thêm" />
                        <input class="submit-gray" name="reset" type="reset" value="Nhập lại" />
                    </fieldset>
                </form>
            </div>
            <!-- End .module-body -->

        </div>
        <!-- End .module -->
        <div style="clear:both;"></div>
    </div>
    <!-- End .grid_12 -->
    <script>
        $(document).ready(function() {
            $('#addAdv').validate({
                rules: {
                    tenquangcao: {
                        required: true,
                    },
                    hinhanh: {
                        required: true,
                    },
                    link: {
                        required: true,
                    },
                },
                messages: {
                    tenquangcao: {
                        required: "<strong>Chưa nhập tên quảng cáo</strong>",
                    },
                    hinhanh: {
                        required: "<strong>Phần quảng cáo phải có hình</strong>",
                    },
                    link: {
                        required: "<strong>Chưa nhập link quảng cáo</strong>",
                    },
                },
            });
        });
    </script>
</div>
<div style="clear:both;"></div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>