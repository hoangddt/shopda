﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>

<?php
        if(isset($_GET['msg'])){
            $tb = $_GET['msg'];
            echo '<strong>'.$tb.'</strong>';
        }

    ?>
    <div class="container_12">


        <div class="bottom-spacing">
            <!-- Button -->
            <div class="float-left">
                <a href="addquangcao.php" class="button">
                    <span>Thêm quảng cáo <img src="/shopda/templates/images/plus-small.gif" alt="Thêm tin"></span>
                </a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="grid_12">
            <!-- Example table -->
            <div class="module">
                <h2><span>Danh sách quảng cáo</span></h2>

                <div class="module-table-body">
                    <form action="delquangcao.php" method="POST" enctype="multipart/form-data">
                        <table id="myTable" class="tablesorter">
                            <thead>
                                <tr>
                                    <th style="width:4%; text-align: center;">STT</th>
                                    <th style="width:35%">Tên</th>
                                    <th style="width:20%">Link</th>
                                    <th style="width:20%; text-align: center;">Hình ảnh</th>
                                    <th style="width:20%; text-align: center;">Vị trí</th>
                                    <th style="width:8%; text-align: center;">Chức năng</th>
                                    <th style="width:6%; text-align: center;">
                                        <a href="" onclick="return confirm('Are you sure you want to delete?');"><input type="submit" name="delete" value="Xóa" /><img src="/shopda/templates/images/bin.gif" width="16" height="16" alt="delete" /></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT * FROM quangcao ORDER BY id_qc DESC";
                                    $result = $mysqli->query($sql);
                                    $i = 1;
                                    while($arr = mysqli_fetch_assoc($result)){
                                        $id_qc = $arr['id_qc'];
                                        $ten = $arr['tenqc'];
                                        $link = $arr['link'];
                                        $hinh = $arr['hinh'];
                                        $vitri = $arr['vitri'];
                                        if($vitri == 1){
                                            $check = "checked = 'checked'";
                                        }else
                                        {
                                            $check = '';
                                        }
                                ?>
                                <tr>
                                    <td class="align-center"><?php echo $i;?></td>
                                    <td><?php echo $ten;?></td>
                                    <td><a href="<?php echo $link;?>"><?php echo $link;?></a></td>
                                    <td align="center"><img src="/shopda/files/<?php echo $hinh;?>" class="hoa" /></td>
                                    <td><input type="radio" name="vitri" value = "<?php echo $id_qc;?>" <?php echo $check;?> onchange = "check(<?php echo $id_qc;?>)" /></td>
                                    <td align="center">
                                        <a href="editqc.php?id_qc=<?php echo $id_qc;?>">Sửa <img src="/shopda/templates/images/pencil.gif" alt="edit" /></a>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" name="<?php echo $id_qc;?>" value="<?php echo $id_qc;?>" />
                                    </td>
                                </tr>
                                <?php
                                    $i++;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </form>
                    <script type="text/javascript">
                    function check(a){
                        $.ajax({
                                    url: 'check.php',
                                    type: 'POST',
                                    cache: false,
                                    data: {id_qc:a},
                                    success: function(data){
                                        $('#myTable').html(data);
                                    },
                                    error: function (){
                                        alert('Có lỗi xảy ra');
                                    }
                                });
                    }
                    </script>
                </div>
                <!-- End .module-table-body -->
            </div>
            <!-- End .module -->
            <div class="pagination">
                <div class="numbers">
                    <span>Trang:</span>
                    <a href="indexAdv.php?page=1" class='current'>1</a>
                    <span>|</span>

                </div>
                <div style="clear: both;"></div>
            </div>

        </div>
        <!-- End .grid_12 -->
    </div>
    <div style="clear:both;"></div>
    
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/templates/inc/footer.php';?>