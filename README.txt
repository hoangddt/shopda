## Đề tài công nghệ web
- Trương Văn Quốc Hoàng
- Lê Minh Huy
 Lớp 13T2 - Nhóm 12

## Cách cài đặt và sử dụng

1. Tạo CSDL tên shopda và import shopda.sql
2. Copy toàn bộ thư mục shopda vào htdocs của apache, bật chức năng .htaccess của apache
3. Chỉnh sửa file config.php và .htaccess để cầu hỉnh đường dẫn chính của trang web
4. Truy cập localhost/shopda
