﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/dbconnect.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/re.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/config.php';?>

<?php
    session_start();
    ob_start();
?>

<!doctype html>
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <title>
       Shop hoa
    </title>
    <meta property="og:type" content="website">
    <meta property="og:title" content="polo-theme">
    <meta property="og:url" content="z">
    <meta property="og:site_name" content="polo-theme">
    <link rel="canonical" href="">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <!-- fonts -->
    <!-- JavaScript -->
    <script src='/shopda/js/jquery-2.1.4.min.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/bootstrap.min.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/parallax.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/common.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/revslider.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/owl.carousel.min.js?1455850384069' type='text/javascript'></script>
	
    <script src='/shopda/js/jgrowl.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/api.jquery.js?137261' type='text/javascript'></script>
    <script src='/shopda/js/cs.script.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/main.js?1455850384069' type='text/javascript'></script>
    <script src='/shopda/js/themgiohang.js' type='text/javascript'></script>
    <!-- Styles -->
    <!-- CSS Style -->
    <link href='/shopda/css/bootstrap.min.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/revslider.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/owl.carousel.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/owl.theme.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/font-awesome.css?1455850384069' rel='stylesheet' type='text/css' />



    <link href='/shopda/css/jgrowl.css?1455850384069' rel='stylesheet' type='text/css' />
    <link href='/shopda/css/styles.css?1455850384069' rel='stylesheet' type='text/css' />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Google Fonts -->
   
    <!-- Header hook for plugins ================================================== -->
    <script>
        var Bizweb = Bizweb || {};
        Bizweb.store = 'polo-theme.bizwebvietnam.net';
        Bizweb.theme = {
            "id": 48701,
            "name": "Polo",
            "role": "main",
            "previewable": true,
            "processing": false,
            "created_on": "2015-11-30T10:06:45Z",
            "modified_on": "2015-11-30T10:07:46Z"
        }
        Bizweb.template = 'index';
    </script>

    <script>
        //<![CDATA[
        (function() {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();

        //]]>
    </script>
    <noscript><iframe height='0' width='0' style='display:none;visibility:hidden' src=''></iframe></noscript>

    <script>
        (function() {
            function asyncLoad() {
                var urls = [];
                for (var i = 0; i < urls.length; i++) {
                    var s = document.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = urls[i];
                    s.src = urls[i];
                    var x = document.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }
            }
            window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
        })();
    </script>

    <script type='text/javascript'>
        (function() {
            var log = document.createElement('script');
            log.type = 'text/javascript';
            log.async = true;
            log.src = '';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(log, s);
        })();
    </script>
	
    <!-- Google Tag Manager -->
    <noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body>
<?php
    if(isset($_GET['msg'])){
        $msg = $_GET['msg'];
        echo "<script>alert('$msg')</script>";
    }
?>
    <div class="page">
        <header class="header-container">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <!-- Header Language -->
                        <div class="col-xs-6">
                            <div class="welcome-msg hidden-xs"> Chào mừng bạn tới PA store </div>
                        </div>
                        <div class="col-xs-6">

                            <!-- Header Top Links -->
                            <div class="toplinks">
                                <div class="links">
                                    <?php
                                        if(isset($_SESSION['taikhoan'])){
                                    ?>
                                    <div class="login"><a title="" href=""><span  class="hidden-xs">Chào,<?php echo $_SESSION['name'];?></span></a></div>
                                    <div class="register"><a title="" href="dangxuat.php"><span  class="hidden-xs">Đăng xuất</span></a></div>
                                    <?php
                                    }
                                    
                                        else
                                        {
                                    ?>
                                        <div class="register"><a title="Register" href="dangky.php"><span  class="hidden-xs">Đăng ký</span></a></div>
                                        <div class="login"><a title="Login" href="dangnhap.php"><span  class="hidden-xs">Đăng nhập</span></a></div>
                                    <?php 
                                                    
                                        }
                                    ?>
                                    
                                </div>
                            </div>
                            <!-- End Header Top Links -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="header container">
                <div class="row">
                    <div class="col-lg-2 col-sm-3 col-md-2">
                        <!-- Header Logo -->
                        <a class="logo" href="<?php echo '/'.ROOT_DIR; ?>/index.php">

                            <img alt="polo-theme" src="/shopda/images/logo.png" />

                        </a>
                        <!-- End Header Logo -->
                    </div>
                    <div class="col-lg-8 col-sm-6 col-md-8">
                        <!-- Search-col -->
                        <div class="search-box">
                            <form action="/search" method="get" id="search_mini_form">
                                <input type="text" placeholder="Tìm kiếm" value="" maxlength="70" class="" name="query" id="search">
                                <button type="submit" id="submit-button" class="search-btn-bg"><span style="white-space: nowrap;">Tìm kiếm</span></button>
                            </form>
                            <div id="result"></div>
                            <script type="text/javascript">
                        $('#search').keyup(function() {
                            search=$(this).val();
                            search=$.trim(search);
                            if(search.length>=2)
                            {
                                $.ajax({
                                    url: 'search.php',
                                    type: 'POST',
                                    cache: false,
                                    data: {searchtxt:search},
                                    success: function(data){
                                        $('#result').html(data);
                                    },
                                    error: function (){
                                        alert('Có lỗi xảy ra');
                                    }
                                });
                            }
                            else
                            {
                                $("#result").children().remove();
                            }
                        });
                            $(":not(#result)").click(function(){
                                    $("#result").children().remove();
                                });
                        
                    </script>
                        </div>
                        <!-- End Search-col -->
                    </div>
                    <!-- Top Cart -->
                    <?php
                        if(isset($_SESSION['giohang'])){
                            $soluong = 0;
                            foreach ($_SESSION['giohang'] as $key => $value) {
                                $soluong = $_SESSION['giohang'][$key]['soluong'] + $soluong;
                            }
                        
                    ?>
                    <div class="col-lg-2 col-sm-3 col-md-2">
                        <div class="top-cart-contain">
                            <div class="mini-cart" id="open_shopping_cart">
                                <div data-hover="dropdown" class="basket dropdown-toggle">
                                    <a href="<?php echo '/'.ROOT_DIR; ?>/giohang.php"> <i class="glyphicon glyphicon-shopping-cart"></i>
                                        <div class="cart-box"><span class="title">Giỏ hàng</span><span id="cart-total">(
                                        <?php
                                            echo $soluong;
                                        ?>

                                        )</span></div>
                                        
                                    </a>
                                </div>
                            </div>
                            <div id="ajaxconfig_info">
                                <a href="#/"></a>
                                <input value="" type="hidden">
                                <input id="enable_module" value="1" type="hidden">
                                <input class="effect_to_cart" value="1" type="hidden">
                                <input class="title_shopping_cart" value="Giỏ hàng" type="hidden">
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- End Top Cart -->
                </div>
            </div>
        </header>
        <!-- end header -->
        <!-- Navbar -->
        <nav>
            <div class="container">
                <div class="nav-inner">
                    <div class="logo-small">
                        <a class="logo" href="<?php echo '/'.ROOT_DIR; ?>index.php">

                            <img alt="polo-theme" src="/shopda/images/logo.png" />

                        </a>
                    </div>
                    <!-- mobile-menu -->
                    <div class="hidden-desktop" id="mobile-menu">
                        <ul class="navmenu">
                            <li>
                                <div class="menutop">
                                    <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
                                    <h2>Danh mục</h2>
                                </div>
                                <ul class="submenu">
                                    <li>
                                        <ul class="topnav">


                                            <li class="level0 nacvv-8 level-top parent">
                                                <a class="level-top" href="<?php echo "/".ROOT_DIR; ?>"> <span>Trang chủ</span> </a>
                                            </li>



                                            <li class="level0 nacvv-8 level-top parent">
                                                <a class="level-top" href="<?php echo "/".ROOT_DIR.'/gioi-thieu.html'; ?>"> <span>Giới thiệu</span> </a>
                                            </li>



                                            <li class="level0 nav-8 level-top parent">
                                                <a class="level-top" href="/san-pham.html"> <span>Sản phẩm</span> </a>
                                                <ul class="level0">
                                                    <?php
                                                        $sql = "SELECT * FROM loaisp";
                                                        $result = $mysqli->query($sql);
                                                        while($arr = mysqli_fetch_assoc($result)){
                                                            $id_loaisp = $arr['id_loaisp'];
                                                            $loaisp = $arr['loaisp'];
                                                            $ten = convertUtf8ToLatin($loaisp);
                                                            $url_ct = "/san-pham/{$ten}-{$id_loaisp}.html";
                                                        
                                                    ?>

                                                    <li class="level1 nav-2-1 first parent">
                                                        <a href="<?php echo $url_ct;?>"> <span><?php echo $loaisp;?></span> </a>
                                                    </li>
                                                    <?php 
                                                        }
                                                    ?>

                                                </ul>
                                            </li>



                                            <li class="level0 nacvv-8 level-top parent">
                                                <a class="level-top" href="/tin-tuc.html"> <span>Tin tức</span> </a>
                                            </li>



                                            <li class="level0 nacvv-8 level-top parent">
                                                <a class="level-top" href="/lien-he.html"> <span>Liên hệ</span> </a>
                                            </li>


                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--navmenu-->
                    </div>
                    <!--End mobile-menu -->
                    <ul id="nav" class="hidden-xs">


                        <li class="level0 parent drop-menu"><a href="<?php echo "/".ROOT_DIR; ?>"><span>Trang chủ</span></a></li>



                        <li class="level0 parent drop-menu"><a href="<?php echo "/".ROOT_DIR.'/gioi-thieu.html'; ?>"><span>Giới thiệu</span></a></li>



                        <li class="level0 parent drop-menu"><a href="<?php echo "/".ROOT_DIR.'/san-pham.html'; ?>"><span>Sản phẩm</span></a>
                            <ul class="level1">
                                <?php
                                    $sql = "SELECT * FROM loaisp";
                                    $result = $mysqli->query($sql);
                                    while($arr = mysqli_fetch_assoc($result)){
                                        $id_loaisp = $arr['id_loaisp'];
                                        $loaisp = $arr['loaisp'];
                                        $ten = convertUtf8ToLatin($loaisp);
                                        $url_ct1 = '/'.ROOT_DIR."/san-pham/{$ten}-{$id_loaisp}.html";
                                    
                                ?>

                                <li class="level1 first parent"><a href="<?php echo $url_ct1;?>"><span><?php echo $loaisp;?></span></a></li>
                                <?php } ?>


                            </ul>
                        </li>



                        <li class="level0 parent drop-menu"><a href="<?php echo "/".ROOT_DIR.'/tin-tuc.html'; ?>"><span>Tin tức</span></a></li>



                        <li class="level0 parent drop-menu"><a href="<?php echo "/".ROOT_DIR.'/lien-he.html'; ?>"><span>Liên hệ</span></a></li>


                    </ul>
                </div>
            </div>
        </nav>
        <!-- end nav -->