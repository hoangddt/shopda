﻿<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/functions/re.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/shopda/config.php';?>

        <!-- header service -->

        <div class="container">
            <div class="header-service">
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="content">
                        <div class="icon-truck">&nbsp;</div>
                        <span><strong>Miễn phí vận chuyển cho đơn hàng trên 1 triệu đồng</strong></span></div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="content">
                        <div class="icon-support">&nbsp;</div>
                        <span><strong>Dịch vụ hỗ trợ khách hàng trực tuyến 24/7</strong></span></div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="content">
                        <div class="icon-money">&nbsp;</div>
                        <span><strong>Hoàn trả hàng miễn phí trong vòng 3 ngày</strong></span></div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="content">
                        <div class="icon-dis">&nbsp;</div>
                        <span><strong>Giảm 10% cho khách mua hàng thường xuyên</strong></span></div>
                </div>
            </div>
        </div>
        <!-- end header service -->
        <!-- main container -->
    <section class="main-container col1-layout home-content-container">
            <div class="container">
                <div class="std">
                    <div class="best-seller-pro">
                        <div class="slider-items-products">
                            <div class="new_title center">
                                <h2>Sản phẩm nổi bật</h2>
                            </div>
                            <div id="best-seller-slider" class="product-flexslider hidden-buttons hidden_btn_cart">
                                <div class="slider-items slider-width-col4">
                                <?php
                                    $sql = "SELECT * FROM sanpham ORDER BY view DESC LIMIT 5";
                                    $result = $mysqli->query($sql);
                                    while( $arr = mysqli_fetch_assoc($result))
                                    {
                                        $id_sp = $arr['id_sp'];
                                        $tensp = $arr['tensp'];
                                        $gia = $arr['giasp'];
                                        $gia = number_format($gia,0,'.','.');
                                        $hinhanh = $arr['hinhanh'];
                                        $ten = convertUtf8ToLatin($tensp);
                                        $url_chitiet = '/'.ROOT_DIR."/chi-tiet-san-pham/{$ten}-{$id_sp}.html";
                                ?>
                                     <div class="item">
                                        <div class="col-item">

                                            <div class="product-image-area">
                                                <a class="product-image" title="<?php echo $tensp;?>" href="<?php echo $url_chitiet;?>">
                                                    <img src="/shopda/files/<?php echo $hinhanh;?>" class="img-responsive" alt="<?php echo $tensp;?>" />
                                                </a>
                                            </div>
                                            <div class="info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="<?php echo $tensp;?>" href="<?php echo $url_chitiet;?>"><?php echo $tensp;?></a> </div>
                                                    <!--item-title-->
                                                    <div class="item-content">

                                                        <div class="price-box">
                                                            <p class="special-price"> <span class="price"><?php echo $gia;?> VNĐ </span> </p>
                                                        </div>

                                                    </div>
                                                    <!--item-content-->
                                                </div>
                                                <!--info-inner-->
                                                <form action="/cart/add" method="post" class="variants" id="product-actions-830911" enctype="multipart/form-data">


                                                    <div class="actions">
                                                        <input type="hidden" name="variantId" value="<?php echo $id_sp;?>" />
                                                        <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                                    </div>

                                                </form>
                                                <div class="clearfix"> </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php 
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php
    $sql_lsp = "SELECT * FROM loaisp";
    $result_lsp = $mysqli->query($sql_lsp);
    while($arr_lsp = mysqli_fetch_assoc($result_lsp))
    {
        $id_loaisp = $arr_lsp['id_loaisp'];
        $loaisp = $arr_lsp['loaisp'];
?>
        <section class="main-container col1-layout home-content-container">
            <div class="container">
                <div class="std">
                    <div class="best-seller-pro">
                        <div class="slider-items-products">
                            <div class="new_title center">
                                <h2><?php echo $loaisp;?></h2>
                            </div>
                            <div id="best-seller-slider" class="product-flexslider hidden-buttons hidden_btn_cart">
                                <div class="slider-items slider-width-col4">
                                <?php
                                    $sql = "SELECT * FROM sanpham WHERE id_loaisp = '$id_loaisp' ORDER BY id_sp DESC ";
                                    $result = $mysqli->query($sql);
                                    while( $arr = mysqli_fetch_assoc($result))
                                    {
                                        $id_sp = $arr['id_sp'];
                                        $tensp = $arr['tensp'];
                                        $gia = $arr['giasp'];
                                        $gia = number_format($gia,0,'.','.');
                                        $hinhanh = $arr['hinhanh'];
                                        $ten = convertUtf8ToLatin($tensp);
                                        $url_chitiet1 = '/'.ROOT_DIR."/chi-tiet-san-pham/{$ten}-{$id_sp}.html";
                                ?>
                                     <div class="item">
                                        <div class="col-item">

                                            <div class="product-image-area">
                                                <a class="product-image" title="<?php echo $tensp;?>" href="<?php echo $url_chitiet1;?>">
                                                    <img src="/shopda/files/<?php echo $hinhanh;?>" class="img-responsive" alt="<?php echo $tensp;?>" />
                                                </a>
                                                
                                            </div>
                                            <div class="info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="<?php echo $tensp;?>" href="<?php echo $url_chitiet1;?>"><?php echo $tensp;?></a> </div>
                                                    <!--item-title-->
                                                    <div class="item-content">

                                                        <div class="price-box">
                                                            <p class="special-price"> <span class="price"><?php echo $gia;?> VNĐ </span> </p>
                                                        </div>

                                                    </div>
                                                    <!--item-content-->
                                                </div>
                                                <!--info-inner-->
                                                <form action="/cart/add" method="post" class="variants" id="product-actions-830911" enctype="multipart/form-data">


                                                    <div class="actions">
                                                        <input type="hidden" name="variantId" value="<?php echo $id_sp;?>" />
                                                        <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                                    </div>

                                                </form>
                                                <div class="clearfix"> </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php 
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End main container -->
<?php
    }
?>
<!-- Latest Blog -->
<section class="latest-blog container">
    <div class="blog-title">
        <h2><span>Tin tức</span></h2>
    </div>
    <?php
        $sql_tt = "SELECT * FROM tintuc";
        $result_tt = $mysqli->query($sql_tt);
        while($arr_tt = mysqli_fetch_assoc($result_tt)){
            $id_tt = $arr_tt['id_tt'];
            $tentt = $arr_tt['tentt'];
            $ha = $arr_tt['hinhanh'];
            $nd = $arr_tt['noidung'];
            $mt = $arr_tt['mota'];
            $ngaydang = $arr_tt['ngaydang'];
            $tachchuoi = explode('-', $ngaydang);
            $ngaydang = $tachchuoi[2].'-'.$tachchuoi[1].'-'.$tachchuoi[0];
            $ten = convertUtf8ToLatin($tentt);
            $url_chitiet3 = '/'.ROOT_DIR."/chi-tiet-tin-tuc/{$ten}-{$id_tt}.html";
            
    ?>
    <div class="col-xs-12 col-sm-4">
        <div class="blog-img">
            <a href="<?php echo $url_chitiet3;?>"><img src="/shopda/files/<?php echo $ha;?>" style = 'width:360p;height:280px;' /></a>
        </div>
        <h2><a href="<?php echo $url_chitiet3;?>"><?php echo $tentt;?></a></h2>
        <div class="post-date"><i class="icon-calendar"></i><?php echo $ngaydang;?></div>

        <p>
            <p style="text-align: justify;"><?php echo $mt;?></p>
        </p>

    </div>
    <?php }?>
</section>
<!-- End Latest Blog -->
