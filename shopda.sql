-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2017 at 11:34 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shopda`
--

-- --------------------------------------------------------

--
-- Table structure for table `chitietdathang`
--

CREATE TABLE IF NOT EXISTS `chitietdathang` (
  `id_dh` int(11) NOT NULL,
  `id_sp` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `thanhtien` int(11) NOT NULL,
  `trangthai` int(11) NOT NULL,
  PRIMARY KEY (`id_dh`,`id_sp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chitietdathang`
--

INSERT INTO `chitietdathang` (`id_dh`, `id_sp`, `soluong`, `thanhtien`, `trangthai`) VALUES
(2, 1, 2, 0, 1),
(4, 2, 4, 0, 1),
(6, 2, 3, 690000, 1),
(6, 4, 1, 250000, 1);

--
-- Triggers `chitietdathang`
--
DROP TRIGGER IF EXISTS `tongtien`;
DELIMITER //
CREATE TRIGGER `tongtien` AFTER INSERT ON `chitietdathang`
 FOR EACH ROW BEGIN
	declare tt int;
	SELECT sum(thanhtien) INTO tt FROM chitietdathang WHERE id_dh = NEW.id_dh;
	UPDATE dathang SET tongtien = tt WHERE id_dh = NEW.id_dh;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dathang`
--

CREATE TABLE IF NOT EXISTS `dathang` (
  `id_dh` int(5) NOT NULL AUTO_INCREMENT,
  `tenkh` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` text COLLATE utf8_unicode_ci NOT NULL,
  `tongtien` int(11) NOT NULL,
  `ngaydat` date NOT NULL,
  `tinhtrang` int(11) NOT NULL,
  `ghichu` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_dh`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `dathang`
--

INSERT INTO `dathang` (`id_dh`, `tenkh`, `sdt`, `diachi`, `tongtien`, `ngaydat`, `tinhtrang`, `ghichu`) VALUES
(1, 'Thui', 'dsfể', '12', 0, '0000-00-00', 1, '13f'),
(4, 'uyen', '13', '13', 0, '0000-00-00', 1, 'fff'),
(6, 'xu', '1', '35', 940000, '2016-04-23', 0, 'et');

-- --------------------------------------------------------

--
-- Table structure for table `giohang`
--

CREATE TABLE IF NOT EXISTS `giohang` (
  `id_gh` int(11) NOT NULL AUTO_INCREMENT,
  `id_kh` int(5) NOT NULL,
  `hinh` text COLLATE utf8_unicode_ci NOT NULL,
  `tensanpham` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `giatien` int(11) NOT NULL,
  `sl` int(11) NOT NULL,
  `tt` int(11) NOT NULL,
  `duyet` int(11) NOT NULL,
  PRIMARY KEY (`id_gh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gioithieu`
--

CREATE TABLE IF NOT EXISTS `gioithieu` (
  `id_gt` int(11) NOT NULL AUTO_INCREMENT,
  `ten` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_gt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gioithieu`
--

INSERT INTO `gioithieu` (`id_gt`, `ten`, `noidung`) VALUES
(1, 'Giới thiệu', '<h1>Giới thiệu</h1>\r\n\r\n<p><strong>I. QU&Aacute; TR&Igrave;NH H&Igrave;NH TH&Agrave;NH V&Agrave; PH&Aacute;T TRIỂN</strong></p>\r\n\r\n<p>Trong những năm qua, x&atilde; hội ph&aacute;t triển, kinh tế tăng trưởng đồng thời l&agrave; chất lượng cuộc sống của người d&acirc;n ng&agrave;y c&agrave;ng c&agrave;ng được n&acirc;ng cao nhiều trung t&acirc;m thương mại, nh&agrave; cao tầng, biệt thự được mọc ra k&egrave;m theo đấy l&agrave; nhu cầu mua sắm c&aacute;c mặt h&agrave;ng phục vụ nhu cầu cuộc sống h&agrave;ng ng&agrave;y như hoa v&agrave; qu&agrave; tặng.</p>\r\n\r\n<p>Polo Sore khai trương si&ecirc;u thị số 442 Đội Cấn, Cống Vị, Ba Đ&igrave;nh, H&agrave; Nội, ch&iacute;nh thức tham gia v&agrave;o lĩnh vực kinh doanh b&aacute;n lẻ trực tuyến, tạo ra một phong c&aacute;ch mua sắm ho&agrave;n to&agrave;n mới với người d&acirc;n thủ đ&ocirc;, th&ocirc;ng qua cung cấp c&aacute;c sản phẩm v&agrave; dịch vụ tới người ti&ecirc;u d&ugrave;ng.</p>\r\n\r\n<p><strong>II. MỤC TI&Ecirc;U CHIẾN LƯỢC</strong></p>\r\n\r\n<p>1. Tối đa ho&aacute; gi&aacute; trị đầu tư của c&aacute;c cổ đ&ocirc;ng; giữ vững tốc độ tăng trưởng lợi nhuận v&agrave; t&igrave;nh h&igrave;nh t&agrave;i ch&iacute;nh l&agrave;nh mạnh;</p>\r\n\r\n<p>2. Kh&ocirc;ng ngừng n&acirc;ng cao động lực l&agrave;m việc v&agrave; năng lực c&aacute;n bộ; Polo Sore&nbsp;phải lu&ocirc;n dẫn đầu ng&agrave;nh b&aacute;n lẻ trong việc s&aacute;ng tạo, ph&aacute;t triển ch&iacute;nh s&aacute;ch đ&atilde;i ngộ v&agrave; cơ hội thăng tiến nghề nghiệp cho c&aacute;n bộ của m&igrave;nh;</p>\r\n\r\n<p>3. Duy tr&igrave; sự h&agrave;i l&ograve;ng, trung th&agrave;nh v&agrave; gắn b&oacute; của kh&aacute;ch h&agrave;ng với Polo Sore; x&acirc;y dựng Polo Sore th&agrave;nh một trong những c&ocirc;ng ty h&agrave;ng đầu Việt Nam c&oacute; chất lượng dịch vụ tốt nhất do kh&aacute;ch h&agrave;ng lựa chọn.</p>\r\n\r\n<p>4. Ph&aacute;t triển Polo Sore th&agrave;nh một trong những c&ocirc;ng ty h&agrave;ng đầu Việt Nam về: quản l&yacute; tốt nhất, m&ocirc;i trường l&agrave;m việc tốt nhất, văn ho&aacute; doanh nghiệp ch&uacute; trọng kh&aacute;ch h&agrave;ng, th&uacute;c đẩy hợp t&aacute;c v&agrave; s&aacute;ng tạo, linh hoạt nhất khi m&ocirc;i trường kinh doanh thay đổi.</p>\r\n\r\n<p><strong>III. THẾ MẠNH V&Agrave; ĐỊNH HƯỚNG CỦA C&Ocirc;NG TY</strong></p>\r\n\r\n<p>Với kim chỉ nam l&agrave; &ldquo;<em>Kh&ocirc;ng ngừng ph&aacute;t triển v&igrave; kh&aacute;ch h&agrave;ng</em>&rdquo;, Polo Sore đ&atilde; quy tụ được Ban l&atilde;nh đạo c&oacute; bề d&agrave;y kinh nghiệm trong lĩnh vực b&aacute;n lẻ, kh&ocirc;ng chỉ mạnh về kinh doanh m&agrave; c&ograve;n mạnh về c&ocirc;ng nghệ, c&oacute; nhiều tiềm năng ph&aacute;t triển, kết hợp với đội ngũ nh&acirc;n vi&ecirc;n trẻ, năng động v&agrave; chuy&ecirc;n nghiệp, tạo n&ecirc;n thế mạnh n&ograve;ng cốt của c&ocirc;ng ty để thực hiện tốt c&aacute;c mục ti&ecirc;u đề ra.</p>\r\n\r\n<p>Hơn nữa, tr&ecirc;n cơ sở nguồn lực của c&ocirc;ng ty v&agrave; nhu cầu của x&atilde; hội, Polo Sore<strong>&nbsp;</strong>lựa chọn ph&aacute;t triển kinh doanh hoa v&agrave; qu&agrave; tặng phục vụ nhu cầu thiết yếu của người d&acirc;n với c&aacute;c sản phẩm đa dạng, phong ph&uacute;, mang lại gi&aacute; trị gia tăng cho người ti&ecirc;u d&ugrave;ng th&ocirc;ng qua c&aacute;c dịch vụ sau b&aacute;n h&agrave;ng.</p>\r\n\r\n<p>Qua qu&aacute; tr&igrave;nh ph&aacute;t triển, b&ecirc;n cạnh việc thiết lập được một hệ thống đối t&aacute;c nước trong nước v&agrave; ngo&agrave;i đến từ c&aacute;c doanh nghiệp lớn, c&oacute; thế mạnh trong lĩnh vực ban..., c&ocirc;ng ty sẽ đầu tư v&agrave;o c&aacute;c ng&agrave;nh nghề mới như bất động sản, khai th&aacute;c kho&aacute;ng, đầu tư t&agrave;i ch&iacute;nh... trong thời gian tới.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE IF NOT EXISTS `khachhang` (
  `id_kh` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tenkh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phanquyen` int(11) NOT NULL,
  PRIMARY KEY (`id_kh`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`id_kh`, `username`, `password`, `tenkh`, `phanquyen`) VALUES
(1, 'nguyenvana', '123456', 'Nguyễn Văn A', 1),
(2, 'nguyenvanb', '123456', 'Nguyễn Văn B', 1),
(3, 'admin', '123123', 'Adminstrator', 0),
(6, 'uyen1', '123', 'uyen', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lienhe`
--

CREATE TABLE IF NOT EXISTS `lienhe` (
  `id_lh` int(5) NOT NULL AUTO_INCREMENT,
  `hoten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_lh`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lienhe`
--

INSERT INTO `lienhe` (`id_lh`, `hoten`, `email`, `noidung`) VALUES
(1, 'Trần A', 'abc@gmail.com', 'xấu'),
(2, 'Trần B', 'abcd@gmail.com', 'xấu'),
(3, 'Nguyễn C', 'abc@gmail.com', 'xấu');

-- --------------------------------------------------------

--
-- Table structure for table `loaisp`
--

CREATE TABLE IF NOT EXISTS `loaisp` (
  `id_loaisp` int(5) NOT NULL AUTO_INCREMENT,
  `loaisp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_loaisp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `loaisp`
--

INSERT INTO `loaisp` (`id_loaisp`, `loaisp`) VALUES
(1, 'Áo da cao cấp'),
(2, 'Ví da'),
(3, 'Túi xách nam'),
(4, 'Giày da nam'),
(5, 'Thắt lưng nam');

-- --------------------------------------------------------

--
-- Table structure for table `phantrang`
--

CREATE TABLE IF NOT EXISTS `phantrang` (
  `id_pt` int(11) NOT NULL AUTO_INCREMENT,
  `admin` int(11) NOT NULL,
  `public` int(11) NOT NULL,
  PRIMARY KEY (`id_pt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `phantrang`
--

INSERT INTO `phantrang` (`id_pt`, `admin`, `public`) VALUES
(1, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `quangcao`
--

CREATE TABLE IF NOT EXISTS `quangcao` (
  `id_qc` int(11) NOT NULL AUTO_INCREMENT,
  `hinh` text COLLATE utf8_unicode_ci NOT NULL,
  `tenqc` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `vitri` bit(1) NOT NULL,
  PRIMARY KEY (`id_qc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `quangcao`
--

INSERT INTO `quangcao` (`id_qc`, `hinh`, `tenqc`, `link`, `vitri`) VALUES
(10, 'vi-da-bo-that-MZ8025-16_1495782846.jpg', 'ví Da', 'http://localhost/shopda/chi-tiet-san-pham/vi-da-bo-that-mz8025-1-51.html', b'0'),
(11, 'download_1495790125.jpg', 'Tình khúc vàng', 'http://localhost/shopda/chi-tiet-san-pham/ao-da-nam-hang-xuat-khau-chau-au-vas66866-(mau-kaki)-56.html', b'0'),
(14, 'that-lung-da-nam-MZ008-555x555_1495790666.jpg', 'Vườn xuân', 'http://localhost/shopda/chi-tiet-san-pham/that-lung-nam-da-xin-mz011-71.html', b'0'),
(15, 'giay-nam-cong-so-cao-cap-MZ134-555x555_1495790419.jpg', 'Sản phẩm khuyến mãi', 'http://localhost/shopda/chi-tiet-san-pham/giay-cong-so-nam-da-tron-mz098-67.html', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE IF NOT EXISTS `sanpham` (
  `id_sp` int(5) NOT NULL AUTO_INCREMENT,
  `id_loaisp` int(11) NOT NULL,
  `tensp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hinhanh` text COLLATE utf8_unicode_ci NOT NULL,
  `mota` text COLLATE utf8_unicode_ci NOT NULL,
  `chitiet` text COLLATE utf8_unicode_ci NOT NULL,
  `giasp` int(10) NOT NULL,
  `view` int(11) NOT NULL,
  PRIMARY KEY (`id_sp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`id_sp`, `id_loaisp`, `tensp`, `hinhanh`, `mota`, `chitiet`, `giasp`, `view`) VALUES
(51, 2, 'VÍ DA BÒ THẬT MZ8025-1', 'vi-da-bo-that-MZ8025-16_1495782846.jpg', 'VÍ DA BÒ THẬT MZ8025-1\r\nMã: MZ8025-1', '', 1323000, 201),
(52, 2, 'BÓP DA NAM CAO CẤP MZ8025-3', 'bop-da-nam-cao-cap-mz8025-3_1495788536.jpg', 'BÓP DA NAM CAO CẤP MZ8025-3\r\nMã: MZ8025-3', '', 1300000, 0),
(53, 2, 'VÍ DA NAM CẦM TAY MZ8095-1', 'vi-da-nam-cam-tay-mz8095-1-555x555_1495789319.jpg', 'VÍ DA NAM CẦM TAY MZ8095-1', '', 1252000, 1),
(54, 2, 'VÍ NAM DA BÒ TỰ NHIÊN MZ8085-1', 'vi-nam-da-bo-tu-nhien-mz8085-1_1495789369.jpg', 'VÍ NAM DA BÒ TỰ NHIÊN MZ8085-1', '<h1>V&Iacute; NAM DA B&Ograve; TỰ NHI&Ecirc;N MZ8085-1</h1>\r\n', 750000, 1),
(55, 2, 'VÍ NAM DA BÒ CAO CẤP ĐEN MZ8070-3', 'vi-nam-da-bo-cao-cap-den-mz8070-3-555x555_1495789528.jpg', 'VÍ NAM DA BÒ CAO CẤP ĐEN MZ8070-3', '<h1>V&Iacute; NAM DA B&Ograve; CAO CẤP ĐEN MZ8070-3</h1>\r\n', 1120000, 0),
(56, 1, 'Áo da nam hàng xuất khẩu Châu Âu VAS66866 - (Màu kaki)', '9q7a749568672_1495789907.jpg', 'Áo da nam hàng xuất khẩu Châu Âu kiểu dáng trẻ trung VAS66866 - (Màu kaki)', '<h1>&Aacute;o da nam h&agrave;ng xuất khẩu Ch&acirc;u &Acirc;u kiểu d&aacute;ng trẻ trung VAS66866 - (M&agrave;u kaki)</h1>\r\n', 750000, 1),
(57, 1, 'Áo da nam sang trọng VAS68078', '9q7a749568672_1495789963.jpg', 'Áo da nam hàng xuất khẩu Châu Âu sang trọng VAS68078 - (Màu kaki)', '<p>&Aacute;o da nam h&agrave;ng xuất khẩu Ch&acirc;u &Acirc;u sang trọng VAS68078 - (M&agrave;u kaki)</p>\r\n', 1000000, 1),
(58, 1, 'Áo da nam sang trọng', '1633255647340035841_1495790029.jpg', 'Áo da nam hàng xuất khẩu Châu Âu sang trọng VAS68078 - (Màu kaki)', '<p>&Aacute;o da nam h&agrave;ng xuất khẩu Ch&acirc;u &Acirc;u sang trọng VAS68078 - (M&agrave;u kaki)</p>\r\n', 8000000, 0),
(59, 1, 'Áo da nam hàng xuất khẩu Châu Âu sang trọng VAS68', 'download_1495790125.jpg', 'Áo da nam hàng xuất khẩu Châu Âu sang trọng VAS68078 - (Màu kaki)', '<p>&Aacute;o da nam h&agrave;ng xuất khẩu Ch&acirc;u &Acirc;u sang trọng VAS68078 - (M&agrave;u kaki)</p>\r\n', 900000, 0),
(60, 3, 'TÚI CLUTCH CẦM TAY MZ6078', 'tui-clutch-cam-tay-mz6078-555x555_1495790191.jpg', 'TÚI CLUTCH CẦM TAY MZ6078', '<h1>T&Uacute;I CLUTCH CẦM TAY MZ6078</h1>\r\n', 1200000, 0),
(61, 3, 'TÚI CLUTCH CẦM TAY MZ6099', 'Tui-Clutch-cam-tay-MZ6078-1-600x400_1495790225.jpg', 'TÚI CLUTCH CẦM TAY MZ6078', '<h1>T&Uacute;I CLUTCH CẦM TAY MZ6078</h1>\r\n', 780000, 0),
(62, 3, 'TÚI CLUTCH CẦM TAY MZ8874', 'Tui-Clutch-cam-tay-MZ6078-4-1-600x400_1495790256.jpg', 'TÚI CLUTCH CẦM TAY MZ6078', '<h1>T&Uacute;I CLUTCH CẦM TAY MZ6078</h1>\r\n', 784000, 0),
(63, 3, 'TÚI CLUTCH CẦM TAY AZ5587', 'vi-nam-da-bo-tu-nhien-mz8085-1_1495790278.jpg', '', '', 900000, 0),
(64, 3, 'TÚI XÁCH DU LỊCH MZ3308-6', 'Tui-xach-du-lich-MZ3308-6-1-555x555_1495790328.jpg', 'TÚI XÁCH DU LỊCH MZ3308-6', '<h1>T&Uacute;I X&Aacute;CH DU LỊCH MZ3308-6</h1>\r\n', 2000000, 0),
(65, 4, 'GIÀY DA NAM CÔNG SỞ MZ133', 'giay-da-nam-cong-so-MZ133-555x555_1495790391.jpg', 'GIÀY DA NAM CÔNG SỞ MZ133', '<h1>GI&Agrave;Y DA NAM C&Ocirc;NG SỞ MZ133</h1>\r\n', 1000000, 0),
(66, 4, 'GIÀY DA NAM CÔNG SỞ MZ134', 'giay-nam-cong-so-cao-cap-MZ134-555x555_1495790419.jpg', 'GIÀY DA NAM CÔNG SỞ MZ134', '', 8000000, 0),
(67, 4, 'GIÀY CÔNG SỞ NAM DA TRƠN MZ098', 'giay-cong-so-nam-da-tron-mz098-1-1-555x555_1495790456.jpg', 'GIÀY CÔNG SỞ NAM DA TRƠN MZ098', '<h1>GI&Agrave;Y C&Ocirc;NG SỞ NAM DA TRƠN MZ098</h1>\r\n', 1500000, 1),
(68, 4, 'GIÀY NAM CÔNG SỞ DA LỘN MZ113', 'giay-nam-cong-so-da-lon-mz113-4-555x555_1495790486.jpg', '', '<h1>GI&Agrave;Y NAM C&Ocirc;NG SỞ DA LỘN MZ113</h1>\r\n', 8000000, 0),
(69, 4, 'GIÀY NAM CÔNG SỞ ĐẾ CAO MZ096', 'giay-nam-cong-so-da-lon-mz113-4-555x555_1495790512.jpg', 'GIÀY NAM CÔNG SỞ ĐẾ CAO MZ096', '<h1>GI&Agrave;Y NAM C&Ocirc;NG SỞ ĐẾ CAO MZ096</h1>\r\n', 1450000, 0),
(70, 5, 'THẮT LƯNG NAM DA BÒ MZ006', 'that-lung-nam-da-bo-mz006-555x555_1495790598.jpg', '', '<h1>THẮT LƯNG NAM DA B&Ograve; MZ006</h1>\r\n', 500000, 0),
(71, 5, 'THẮT LƯNG NAM DA XỊN MZ011', 'that-lung-nam-da-xin-MZ011-555x555_1495790633.jpg', '', '<h1>THẮT LƯNG NAM DA XỊN MZ011</h1>\r\n', 8000000, 1),
(72, 5, 'THẮT LƯNG DA NAM MZ008', 'that-lung-da-nam-MZ008-555x555_1495790666.jpg', 'THẮT LƯNG DA NAM MZ008', '', 600000, 0),
(73, 5, 'DÂY LƯNG NAM DA BÒ MZ002-1', 'That-lung-day-lung-nam-da-bo-mz002-1-6-555x555_1495790693.jpg', '', '<h1>D&Acirc;Y LƯNG NAM DA B&Ograve; MZ002-1</h1>\r\n', 500000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tintuc`
--

CREATE TABLE IF NOT EXISTS `tintuc` (
  `id_tt` int(11) NOT NULL AUTO_INCREMENT,
  `tentt` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ngaydang` date NOT NULL,
  `hinhanh` text COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tintuc`
--

INSERT INTO `tintuc` (`id_tt`, `tentt`, `ngaydang`, `hinhanh`, `mota`, `noidung`) VALUES
(1, '10 loại socola ngon nhất thế giới', '2016-04-14', 'socola.jpg', 'Bạn đang muốn chọn một loại chocolate tuyệt ngon dành tặng người yêu? Vậy hãy cùng xem những gợi ý dưới đây từ Quà Tặng Phụ Nữ nhé! \r\nTừ những thanh chocolate đen nguyên chất, cho tới kẹo chocolate có nhân rượu màu, hay những thỏi chocolate đắng, tất cả đều ngon đến ngỡ ngàng. Bạn sẽ chẳng bao giờ hối tiếc khi lỡ đam mê các loại bánh kẹo của những hãng này:', '1. Teuscher (Zurich, Thụy Sĩ)\r\nChocolate Teuscher được bắt đầu sản xuất từ hơn 70 năm trước, tại một thị trấn nhỏ trong dãy Alps (Thụy Sĩ). Dolf Teuscher đã đi khắp thế giới để tìm những loại ca cao ngon nhất, cũng như tìm ra các loại bột hạnh nhân, hoa quả, các loại hạt và phụ liệu phù hợp để có thể làm được những viên kẹo của riêng mình. Sau nhiều năm thử nghiệm, ông đã hòa trộn được những thành phần ấy trong công thức làm chocolate nổi tiếng ngày nay.\r\nChocolate Teuscher hiện giờ có hơn 100 loại khác nhau, tất cả đều có chung một công thức nguyên thủy được truyền lại từ đời bố sang đời con.\r\nChỉ những loại thành phần tự nhiên đắt giá nhất mới được sử dụng, và tuyệt đối không có một chút hóa chất, phụ gia hay chất bảo quản nào được thêm vào. Hàng tuần, những thanh chocolate từ cửa hàng của Teuscher được chuyển đi khắp thế giới.\r\n\r\n2. Vosges Haut-Chocolat (Chicago, Illinois, Mỹ)\r\nBà chủ Katrina Markoff, cũng là nghệ nhân làm chocolate rất kỹ tính trong việc lựa chọn các loại phụ liệu, các loại hoa, cũng như các loại chocolate để đưa vào gian bếp làm bánh của hãng Vosges. Bà đã học nghệ thuật làm chocolate tại hàng làm bánh Le Cordon Bleu ở Paris (Pháp).\r\n\r\n3. Chocolate Scharffen Berger (Berkeley, California, Mỹ)\r\nNổi tiếng với những thanh chocolate đen, Scharffen Berger Maker là nhà sản xuất "kẹo đen" hàng đầu thế giới. Các quy trình sản xuất được thực hiện độc lập, để bảo đảm rằng hương vị chocolate ở đây không giống bất kỳ nơi nào khác.\r\nNhững thanh chocolate được làm từ loại ca cao tốt nhất thế giới, cùng quy trình kiểm nghiệm cẩn trọng và cách rang xay độc đáo. Tất cả những viên chocolate được sản xuất theo từng mẻ nhỏ, bằng phương pháp hoàn toàn thủ công.\r\nCùng với loại chocolate ăn ngay thường gặp, Scharffen Berger còn có cả loại chocolate thực phẩm (hay còn gọi là chocolate đắng).\r\n\r\n4. Hãng chocolate Jacques Torres (New York, Mỹ)\r\nNếu đặt chân vào cửa hàng chocolate Jacques Torres, bạn sẽ cảm thấy như thể mình đang bước vào một cửa hàng bán đồ chuyên dụng nhỏ ở châu Âu. Rất nhiều khách hàng đã so sánh trải nghiệm này với cảm giác khi xem bộ phim Chocolat Jacques được làm thủ công từ những thanh chocolate tươi.\r\nHãy nếm luôn tại đây, những chiếc ghế trong cửa hàng luôn được chào đón bạn ngồi xuống, nhấm nháp từng ngụm chocolate nóng, và thưởng thức những chiếc bánh nướng nhân chocolate mới làm, hoặc đặt mua một gói mang về.\r\nKhách đến cửa hàng sẽ được xem tận mắt quá trình làm những viên kẹo sô cô la qua khung cửa kính. Có tất cả năm cửa hàng chocolate Jacques Torres ở NewYork và một cửa hàng ở Harrah thành phố Atlantic.\r\n\r\n5. Hãng bánh kẹo Norman Love (Ft. Myers, Florida, Mỹ)\r\n“Chocolate là niềm đam mê của tôi” - đó là lời của Norman Love, người luôn mơ về việc làm ra những thanh chocolate không chỉ hấp dẫn mà còn thực sự ngon lành. Ông Love và một cộng sự đã hoàn thiện kỹ thuật vẽ màu cho từng viên kẹo hoặc vẽ bằng tay một cách thủ công, hoặc dùng cách sơn airbrush.\r\nNhững khuôn làm kẹo sau khi sơn vẽ sẽ được rót đầy những loại chocolate hàng đầu của Bỉ, Pháp và Thụy Sĩ. Kẹo chocolate bí ngô trắng là loại hấp dẫn nhất.\r\nNhững loại kẹo khác của Norman Love cũng đều có thành phần rất tinh khiết như quả mâm xôi, chuối, gừng, caramel, chanh leo, hạt dẻ…\r\n\r\n6. Valrhona (Pháp)\r\nHãng Valrhona đã không ngừng tạo ra những loại chocolate ngon bậc nhất thế giới từ những năm 1922. Hạt ca cao được hãng đặt mua trực tiếp từ những đồn điền hàng đầu ở Nam Mỹ, Caribê, và khu vực Thái Bình Dương.\r\nValrhone cũng là một trong những hãng sản xuất chocolate đầu tiên giới thiệu sản phẩm của mình giống như các hãng rượu đã làm. Những thanh chocolate được gắn nhãn như: grand cru (theo vùng đất có nguyên liệu tốt nhất), vintage (theo độ tuổi sản phẩm), single origins (có nguồn gốc sản phẩm từ một vùng đơn lẻ), hay single estate …\r\nTừ năm 2008, hãng này còn giới thiệu loại chocolate Xocopili có vị cay và mặn.\r\n\r\n7. Hãng chocolate Godiva (Brussels, Bỉ và trên toàn thế giới)\r\nNhững thanh chocolate đầu tiên của hãng Godiva được làm vào năm 1920, trong một cửa hàng làm chocolate và bánh kẹo của gia đình nhà Draps ở thành phố Brussels, nước Bỉ.\r\nNăm 14 tuổi, Joseph Draps bắt đầu tham gia vào công việc kinh doanh của gia đình. Sau nhiều năm, ông đã phát huy hết năng lực và sực sáng tạo của mình trong vai trò một nghệ nhân làm chocolate, cũng như một doanh nhân tài năng.\r\nÔng quyết định tạo ra hàng loạt những loại chocolate nổi tiếng, và đặt cho chúng những cái tên rất dễ gợi nhớ. Cũng chính ông đã chọn tên gọi “Godiva” và đặt những viên chocolate của mình trong những chiếc hộp bằng vàng khiến ai cũng có thể nhận ra ngay tức thì. Chất lượng tuyệt hảo của chocolate Godiva được ghi nhận bằng việc họ được chọn là nhà cung cấp chính cho toàn nước Bỉ.\r\nGodiva hiện vẫn tiếp tục là hãng sản xuất chocolate danh tiếng nhất thế giới.\r\n\r\n8. Chocolate Richard Donnelly (Santa Cruz, California, Mỹ)\r\nNhững thanh chocolate của hãng này không giống bất kỳ loại chocolate nào. Richard Donnelly rất thích tạo ra những trải nghiệm mới bằng cách kết hợp những vị mạnh và gắt nhất. Ông dùng các loại chocolate của Bỉ và Pháp, trong thành phần kết hợp có các hương vị như oải hương, ớt hun khói, nghệ tây, bạch đấu khấu, trà Earl Grey.\r\nNhững sáng tạo độc đáo này đã giúp cho Donnelly giành được giải thưởng “Thợ thủ công bậc thầy” tại triển lãm chocolate toàn châu Âu danh giá ở thành phố Perugia, nước Ý. Cần nhớ rằng, ở thời điểm đó, cửa hàng của Donnelly mới chỉ có mười năm kinh nghiệm trên thương trường.\r\nĐể bảo đảm những thanh chocolate của mình luôn có chất lượng tốt nhất, cũng như luôn tươi mới nhất, hãng này chỉ sản xuất tối đa mỗi ngày 22,5kg chocolate. Nếu bạn muốn có những trải nghiệm đột phá từ những loại hương liệu độc đáo khác thường, hãy thử loại chocolate trắng với nhân hạt macadamia hay loại chocolate caramel với hương vani mật ong.\r\nNhững lợi socola ngon nhất thế giới \r\n\r\n9. Richart (Paris, Pháp)\r\nChú trọng vào chất lượng sản phẩm, hãng chocolate Richart của Pháp bảo đảm rằng bạn sẽ được thưởng thức những thanh chocolate nguyên chất nhất làm từ các loại hương liệu thành phần nguyên chất nhất. Những thanh chocolate của hãng này được làm theo công thức do chính gia đình nhà Richart đã phát triển và thử nghiệm.\r\nGia đình này nổi tiếng vì đã giành được bảy lần huy chương Ruban Bleu dành cho hãng sản xuất bánh kẹo uy tín nhất nước Pháp.\r\nĐã đạt đến đỉnh cao trong nghệ thuật làm chocolate, hãng Richart hiện giờ đang tập trung vào hướng thay đổi các loại hương liệu gia giảm, cũng như chú trọng vào việc tạo ra các sản phẩm có màu sắc và thiết kế đặc trưng.\r\nMột hộp chocolate đủ vị có giá đủ khiến bạn có thể giật mình. Bạn có sẵn sàng chi 850 USD để có được bảy phần chocolate đóng trong hộp gỗ, kèm theo cả đồng hồ đo nhiệt độ và độ ẩm?\r\n\r\n10. Puccini Bomboni (Amsterdam, Hà Lan)\r\nBạn cần phải đến Hà Lan để thưởng thức loại chocolate ngon nhất của đất nước này. Lý do là bởi hãng không hề có địa chỉ phân phối nào khác. Puccina Bomboni ban đầu vốn là một nhà hàng và hiệu cà phê nổi tiếng. Những người chủ của hãng sô cô la này tự tay làm từng mẻ chocolate ngay tại đây.\r\nSự kết hợp độc đáo giữa chocolate và các loại hương liệu, tất cả cùng được pha chế theo những công thức mới là điểm mang lại phong cách cho loại chocolate này. Mặc dù không phong phú về chủng loại, thế nhưng, chất lượng của chocolate Puccini Bomboni thực sự rất hấp dẫn.'),
(5, 'Gợi ý chọn hoa làm quà tặng', '2016-04-25', 'tinh-yeu-ngot-ngao-2_1461574328.jpg', 'Tặng hoa là gửi đi một thông điệp ý nghĩa thường mang lại cảm xúc của tình yêu và hạnh phúc bằng hương thơm và vẻ đẹp tuyệt đối của những bông hoa. Kèm theo hoa những tấm thiệp chân thành tự làm, sôcôla và các quà tặng thủ công khác. ', '<p>Tặng hoa l&agrave; gửi đi một th&ocirc;ng điệp &yacute; nghĩa thường mang lại cảm x&uacute;c của t&igrave;nh y&ecirc;u v&agrave; hạnh ph&uacute;c bằng hương thơm v&agrave; vẻ đẹp tuyệt đối của những b&ocirc;ng hoa. K&egrave;m theo hoa những tấm thiệp ch&acirc;n th&agrave;nh tự l&agrave;m, s&ocirc;c&ocirc;la v&agrave; c&aacute;c qu&agrave; tặng thủ c&ocirc;ng kh&aacute;c. X&aacute;c định loại th&ocirc;ng điệp bạn muốn gửi trước khi lựa chọn b&oacute; hoa. Hoa hồng m&agrave;u đỏ v&agrave; m&agrave;u hồng thường gửi đến một đối tượng l&atilde;ng mạn, trong khi hoa hồng hoặc hoa kh&aacute;c m&agrave;u v&agrave;ng gửi đến cho bạn b&egrave;.<img alt="" src="https://bizweb.dktcdn.net/100/037/483/files/0808539tinh-yeu-ngot-ngao-1-2a5dd5f0-a360-41cb-8169-77bbd9fa37a8.jpg?v=1449541567150" style="height:500px; width:500px" /></p>\r\n\r\n<p>Những b&ocirc;ng hoa nhiệt đới c&oacute; một sự tinh tế kỳ lạ. Những b&ocirc;ng hoa n&agrave;y kỳ lạ v&agrave; c&oacute; c&aacute;c đốm, nhưng h&igrave;nh dạng tuyệt đẹp. Mua b&oacute; hoa n&agrave;y cho một người bạn, th&agrave;nh vi&ecirc;n gia đ&igrave;nh hoặc bạn đời. T&igrave;m kiếm c&aacute;c kiểu b&oacute; hoa huong duong c&oacute; th&ecirc;m những b&ocirc;ng hoa rực rỡ, chẳng hạn như hoa huệ c&oacute;c, hoa lan v&agrave; hoa d&acirc;m bụt đầy m&agrave;u sắc. Bạn c&oacute; thể tập trung v&agrave;o một loại hoa nhiệt đới hoặc kết hợp bằng một số lo&agrave;i nổi bật.</p>\r\n\r\n<p>Hoa hồng bảy sắc cầu vồng</p>\r\n\r\n<p>Mua một b&oacute; hoa hồng bảy sắc cầu vồng cho một người n&agrave;o đ&oacute; m&agrave; bạn quan t&acirc;m s&acirc;u sắc. B&oacute; hoa n&agrave;y bao gồm nhiều loại hoa cam tu cau, v&igrave; vậy c&oacute; thể gửi đến một người y&ecirc;u hoặc người bạn. Kết hợp hoa hồng v&agrave;ng, hồng phấn, đỏ, hồng đậm, kem v&agrave; trắng trong b&oacute; hoa. Sự kết hợp n&agrave;y thường kh&ocirc;ng được t&igrave;m thấy tại c&aacute;c cửa h&agrave;ng hoa, v&igrave; vậy bạn c&oacute; thể sẽ cần phải y&ecirc;u cầu người b&aacute;n đặt c&aacute;c m&agrave;u sắc kh&aacute;c nhau v&agrave;o b&oacute; hoa trang tr&iacute; n&agrave;y. K&egrave;m theo s&ocirc;c&ocirc;la v&agrave; b&oacute;ng bay nếu ng&acirc;n s&aacute;ch của bạn cho ph&eacute;p.</p>\r\n'),
(7, 'Thuật phong thủy và quà tặng', '2016-04-25', 'url-1_1461574486.jpg', 'Người ta thường có câu: “Của cho không bằng cách cho”, ý nói sự khôn khéo và tinh tế trong giao tiếp là điều quan trọng hơn cả giá trị vật chất mà con người ta dành cho nhau. Và việc tặng quà cũng không là ngoại lệ.', '<p>Người ta thường c&oacute; c&acirc;u: &ldquo;Của cho kh&ocirc;ng bằng c&aacute;ch cho&rdquo;, &yacute; n&oacute;i sự kh&ocirc;n kh&eacute;o v&agrave; tinh tế trong giao tiếp l&agrave; điều quan trọng hơn cả gi&aacute; trị vật chất m&agrave; con người ta d&agrave;nh cho nhau. V&agrave; việc tặng qu&agrave; cũng kh&ocirc;ng l&agrave; ngoại lệ.</p>\r\n\r\n<p><img alt="" src="https://bizweb.dktcdn.net/100/037/483/files/url-1.jpg?v=1449476169647" style="height:334px; width:500px" /></p>\r\n\r\n<p>Bạn h&atilde;y l&agrave;m thế n&agrave;o để cho m&oacute;n qu&agrave; của &nbsp;m&igrave;nh trở n&ecirc;n thật &yacute; nghĩa trước người nhận, v&agrave; cũng đừng qu&ecirc;n tạo cho người nhận cảm gi&aacute;c vui vẻ v&agrave;&nbsp;kh&ocirc;ng miễn cưỡng.</p>\r\n');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
