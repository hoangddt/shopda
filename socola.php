<?php include $_SERVER["DOCUMENT_ROOT"].'/shopda/include/header.php';?>

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <ul>
                <li class="home"> <a href="/" title="Trang chủ">Trang chủ</a><span>—›</span></li>


                <li><strong>Hoa ngày 20/10</strong></li>


            </ul>
        </div>
    </div>
</div>

<!-- Two columns content -->
<div class="main-container col2-left-layout">
    <div class="main container">
        <div class="row">
            <section class="col-main col-sm-9 col-sm-push-3">
                <div class="category-description std">
                    <div class="slider-items-products">
                        <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                            <div class="slider-items slider-width-col1">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="category-title">
                    <h1>Hoa ngày 20/10</h1>
                </div>
                <div class="category-products">

                    <div class="toolbar">
                        <div class="sorter">
                            <div class="view-mode">
                                <span title="Grid" class="button button-active button-grid"></span>
                                <a href="?view=list" title="List" class="button button-list"></a>
                            </div>
                        </div>
                        <div id="sort-by">
                            <label class="left">Lọc theo: </label>
                            <select name="sortBy" id="sortBy" class="selectBox" style="padding: 0px 10px; height: 30px;">
								<option selected value="default">Mặc định</option>
								<option value="alpha-asc">A &rarr; Z</option>
								<option value="alpha-desc">Z &rarr; A</option>
								<option value="price-asc" >Giá tăng dần</option>
								<option value="price-desc">Giá giảm dần</option>
								<option value="created-desc">Hàng mới nhất</option>
								<option value="created-asc">Hàng cũ nhất</option>
							</select>
                            <script>
                                Bizweb.queryParams = {};
                                if (location.search.length) {
                                    for (var aKeyValue, i = 0, aCouples = location.search.substr(1).split('&'); i < aCouples.length; i++) {
                                        aKeyValue = aCouples[i].split('=');
                                        if (aKeyValue.length > 1) {
                                            Bizweb.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
                                        }
                                    }
                                }

                                $(function() {
                                    $('#sortBy')
                                        // select the current sort order
                                        .val('default')
                                        .bind('change', function() {
                                            Bizweb.queryParams.sortby = jQuery(this).val();
                                            location.search = jQuery.param(Bizweb.queryParams).replace(/\+/g, '%20');
                                        });
                                });
                            </script>
                            <a class="button-asc left" href="#" title="Set Descending Direction"><span class="glyphicon glyphicon-arrow-up"></span></a>
                        </div>
                    </div>

                    <ul class="products-grid hidden_btn_cart">

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Nắng ngập tràn" href="/nang-ngap-tran">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/2652627sunshine_1[1].jpg?v=1448877839017" class="img-responsive" alt="Nắng ngập tràn" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830908" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273185" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Nắng ngập tràn" href="/nang-ngap-tran">Nắng ngập tràn</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">450.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830908" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273185" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Tình yêu ngọt ngào" href="/tinh-yeu-ngot-ngao">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/0808539tinh_yeu_ngot_ngao_1[1].jpg?v=1448877839617" class="img-responsive" alt="Tình yêu ngọt ngào" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830911" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273195" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Tình yêu ngọt ngào" href="/tinh-yeu-ngot-ngao">Tình yêu ngọt ngào</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">1.200.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830911" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273195" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Thành công" href="/thanh-cong">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/0214077thanh_cong_1[1].jpg?v=1448877839577" class="img-responsive" alt="Thành công" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830912" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273196" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Thành công" href="/thanh-cong">Thành công</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">950.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830912" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273196" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Nắng rộn rã" href="/nang-ron-ra">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/5407081nang_ron_ra_1[1].jpg?v=1448877839043" class="img-responsive" alt="Nắng rộn rã" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830913" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273192" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Nắng rộn rã" href="/nang-ron-ra">Nắng rộn rã</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">50.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830913" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273192" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Hương mùa xuân" href="/huong-mua-xuan">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/5136172huong_mua_xuan_1[1].jpg?v=1448877839527" class="img-responsive" alt="Hương mùa xuân" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830915" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273191" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Hương mùa xuân" href="/huong-mua-xuan">Hương mùa xuân</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">550.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830915" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273191" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Ấm áp yêu thương" href="/am-ap-yeu-thuong">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/4214252am_ap_yeu_thuong_1[1].jpg?v=1448877839557" class="img-responsive" alt="Ấm áp yêu thương" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830916" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273198" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Ấm áp yêu thương" href="/am-ap-yeu-thuong">Ấm áp yêu thương</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">450.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830916" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273198" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Đượm tình" href="/duom-tinh">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/1847956duom_tinh_1[1].jpg?v=1448877839580" class="img-responsive" alt="Đượm tình" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830920" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273201" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Đượm tình" href="/duom-tinh">Đượm tình</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">550.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830920" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273201" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Tình khúc vàng" href="/tinh-khuc-vang">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/2556022gold_love_1[1].jpg?v=1448877839537" class="img-responsive" alt="Tình khúc vàng" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830918" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273200" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Tình khúc vàng" href="/tinh-khuc-vang">Tình khúc vàng</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">450.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830918" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273200" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                        <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">





                            <div class="col-item">

                                <div class="product-image-area">
                                    <a class="product-image" title="Bữa tiệc hoa hồng" href="/bua-tiec-hoa-hong">
                                        <img src="//bizweb.dktcdn.net/thumb/large/100/037/483/products/2432507party_1[1].jpg?v=1448877839617" class="img-responsive" alt="Bữa tiệc hoa hồng" />
                                    </a>
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830917" enctype="multipart/form-data">


                                        <div class="hover_fly">
                                            <a class="exclusive ajax_add_to_cart_button btn-cart add_to_cart" href="#" title="Cho vào giỏ hàng">
                                                <div><i class="icon-shopping-cart"></i><span>Cho vào giỏ hàng</span></div>
                                            </a>
                                            <input type="hidden" name="variantId" value="1273199" />
                                        </div>

                                    </form>
                                </div>
                                <div class="info">
                                    <div class="info-inner">
                                        <div class="item-title"> <a title="Bữa tiệc hoa hồng" href="/bua-tiec-hoa-hong">Bữa tiệc hoa hồng</a> </div>
                                        <!--item-title-->
                                        <div class="item-content">

                                            <div class="price-box">
                                                <p class="special-price"> <span class="price">850.000&#8363; </span> </p>
                                            </div>

                                        </div>
                                        <!--item-content-->
                                    </div>
                                    <!--info-inner-->
                                    <form action="/cart/add" method="post" class="variants" id="product-actions-830917" enctype="multipart/form-data">


                                        <div class="actions">
                                            <input type="hidden" name="variantId" value="1273199" />
                                            <button class="button btn-cart btn-cart add_to_cart" title="Cho vào giỏ hàng" type="button"><span>Cho vào giỏ hàng</span></button>
                                        </div>

                                    </form>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <div class="toolbar">
                        <div class="pager">
                            <div class="pages">
                                <label>Trang:</label>
                                <ul class="pagination">




                                    <li class="active"><a href="#" style="pointer-events:none">1</a></li>




                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
            <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
                <div class="side-nav-categories">
                    <div class="block-title"> Danh mục sản phẩm </div>
                    <!--block-title-->
                    <!-- BEGIN BOX-CATEGORY -->
                    <div class="box-content box-category">
                        <ul>


                            <li> <a href="/san-pham-khuyen-mai">Sản phẩm khuyến mại</a> </li>



                            <li> <a href="/san-pham-noi-bat">Sản phẩm nổi bật</a> </li>



                            <li> <a href="/hoa-tuoi-chuc-mung">Hoa chúc mừng</a> </li>



                            <li> <a class="active" href="/hoa-ngay-20-10">Hoa ngày 20/10</a> </li>



                            <li> <a href="/hoa-ngay-08-3">Hoa ngày 08/3</a> </li>



                            <li> <a href="/hoa-tuoi-sinh-nhat">Hoa sinh nhật</a> </li>



                            <li> <a href="/hoa-tuoi">Hoa tươi cưới</a> </li>



                            <li> <a href="/hoa-tuoi-tinh-yeu">Hoa tình yêu</a> </li>



                            <li> <a href="/qua-tang">Quà tặng</a> </li>



                            <li> <a href="/socola">Socola</a> </li>


                        </ul>
                    </div>
                    <!--box-content box-category-->
                </div>
                <div class="block block-cart hide_scroll open_button" id="open_shopping_cart">
                    <div class="block-title"><span>Giỏ hàng của bạn</span></div>
                    <div class="block-content">
                        <div class="summary">
                            <p class="amount">Đang có <span id="cart-total">8</span> sản phẩm trong giỏ hàng của bạn.</p>
                            <p class="subtotal"> <span class="label">Tổng tiền:</span> <span class="price total_price">5.230.000&#8363;</span> </p>
                        </div>
                        <div class="ajax-checkout">
                            <button onclick="window.location='/checkout'" type="submit" title="Submit" class="button button-checkout"><span>Thanh toán</span></button>
                        </div>
                        <ul class="shopping_cart style_cart_col">
                        </ul>
                    </div>
                </div>


                <div class="block block-banner">
                    <a href="#"><img src="//bizweb.dktcdn.net/100/037/483/themes/48701/assets/block-banner.png?1455850384069" alt="block-banner"></a>
                </div>

            </aside>
        </div>
    </div>
</div>
<?php include $_SERVER["DOCUMENT_ROOT"].'/shopda/include/footer.php';?>